<?php
require './../app/App.php';
$C = new Conf(new ConfSpec());

$log = new Logger();
$users = new Users(
	$C->sessionExpire, //session expiration - renew interval
	$C->secureCookie,  //force SSL cookies
	$C->passwdFile,    //path for file contains users crecedntials
	$C->minUsernameLen, //min length for username
	$C->minPasswdLen    //min length for passwd
);

$user_req = new Req(); //curl request Req class for login and upload certificates to Wedos 
$install = new Cinstall($user_req, $log); //Wedos certificate installer

$model = new Model($C, $log, $users, $install);
$view = new View();
$controler = new Controller($model, $view);

//start build app
$app = new App($controler);

//routes ($method, $path, $secured, $csrf)
$app->route('GET', '/', 'index', true); //main index home page
$app->route('GET', '/certinfo', 'infoUrlCert', true); //ajax info about live certificate - AJAX
$app->route('GET', '/peminfo', 'infoPemCert', true); //ajax info about local pem certificate - AJAX
$app->route('GET', '/getkey', 'viewKey', true); //ajax get pem key for manual copy - AJAX
$app->route('GET', '/run', 'leGen', true, true); //ajax start generating new ssl certifikates - AJAX CSRF -
$app->route('GET', '/install', 'leInstall', true, true); //ajax install generated cert to Wedos - AJAX CSRF -
$app->route('GET', '/archive', 'zipCertFiles', true, true); //ajax start zip generated ssl certifikates - AJAX CSRF -
$app->route('GET', '/getzip', 'zipDownload', true); //ajax download zipped certiuficates - AJAX CSRF -
$app->route('GET', '/login', 'userLogin', false); //login form for existing or create first user
$app->route('POST', '/auth', 'userAuth', false, true); //auth login form submit - CSRF
$app->route('POST', '/firstuser', 'firstUser', false, true); //create first user (admin) form submit - CSRF
$app->route('GET', '/logout', 'userLogout', true); //logout - switch to public web
$app->route('GET', '/logged/as', 'loggedUser', true); //ajax fragment logged user and session expiration
$app->route('GET', '/conf', 'editConf', true); //show configuration editor form
$app->route('POST', '/conf/save', 'saveConf', true, true); //save configuration changes - CSRF

//user profiles page and ajax - ?ajax=yes
$app->route('GET', '/user/list', 'listUsers', true);
//all user api crud operations using ajax json request/response
$app->route(['POST', 'PUT', 'DELETE'], '/user/crud', 'editUser', true, true);
//cron auto renew and mail report certificate 
$app->route('GET', '/cron', 'leCron', false);
$app->run();
