/*jshint maxerr: 10000 */
function Fns() {"use strict";}

Fns.prototype.ajax = function ajax(conf, cb) {
	conf = (typeof conf === 'object' && conf) ? conf : {};
	conf.url = (typeof conf.url === 'string' && conf.url.length > 0) ?
		conf.url : location.href;
	conf.method = conf.method = (typeof conf.method === 'string' && conf.method.length > 0) ?
		conf.method.toUpperCase() : 'GET';
	conf.json = (typeof conf.json === 'boolean') ? conf.json : false;
	conf.data = (typeof conf.data === 'string' && conf.data.length > 0) ?
		conf.data : false;
	conf.timeout = conf.timeout = parseInt(conf.timeout, 10);
	conf.timeout = (conf.timeout > 0) ? conf.timeout : 0; //default without timeout
		
	var xhr = new XMLHttpRequest();
  xhr.open(conf.method, conf.url, true); //open async request
	
	//set request headers
	if(typeof conf.headers === 'object' && conf.headers) {
		for(var he in conf.headers) {
			if(conf.headers.hasOwnProperty(he)) {
				if(typeof conf.headers[he] === 'string' && conf.headers[he].length > 0) {
					xhr.setRequestHeader(he, conf.headers[he]);
				}
			}
		}
	}
	
	var self = this;
	
	// x-requested-with
	xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	//add json header
	if(conf.json) xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
	//csrf token
	var token = document.querySelector('#csrf-token');
	if(token) {
	  var csrf = token.getAttribute('content');
	  xhr.setRequestHeader('X-Csrf-Token', csrf);
	}
	
	//progress events if supported
  if('onprogress' in xhr && typeof cb === 'function') {
  	//xhr.setRequestHeader('X-Accel-Buffering', 'no');
		xhr.onprogress = function() {
  		return cb(false, xhr.responseText, false);
  	};
  }
	
	//timeouts use XHR2 or setTimeout for xhr1
	if(conf.timeout > 0) {
		var tmargs = ['Request imeout! Time is > configured: ' + conf.timeout +
			'ms ! Try to increshe or set to 0!', xhr.response, true];
		
		if(false && 'timout' in xhr) { //timeout handler XHR2 only
			xhr.timeout = conf.timeout; //set timeout
			xhr.ontimeout = function() {
				return cb.apply(tmargs);
			};
		} else { //timeout for XHR1
			xhr.timeoutFallback = setTimeout(function() {
				xhr.onreadystatechange = null;
				xhr.abort();
				return cb.apply(this, tmargs);
			}, conf.timeout);	
		}
	}
  
  //check request done
  xhr.onreadystatechange = function() {
    var status, statusText, data;
    if (xhr.readyState == 4) { // `DONE`
      status = xhr.status;
      statusText = xhr.statusText;
      data = xhr.response;
      
      if('timeoutFallback' in xhr) clearTimeout(xhr.timeoutFallback);
      
      //refresh csrf token
      var headers = self.parseHeaders(xhr.getAllResponseHeaders());
      var upd_header = headers['X-Csrf-Token-Update'.toLowerCase()];
      if(upd_header) {
        token.setAttribute('content', upd_header);
      }
      
      if (status === 200) {
        if(typeof cb === 'function') return cb(false, data, true);
      } else {
        if(typeof cb === 'function') return cb(statusText, data, true);
      }
    }
	};
	
	//send request and data
	if (conf.data && conf.type !== 'GET') {
		xhr.send(conf.data);
	} else {
		xhr.send();
	}
};

Fns.prototype.parseHeaders = function parseHeaders(headstr) {
  var headers = {};
  var sh = headstr.split('\n');
  for(var i = 0; i < sh.length; i++) {
    var he = sh[i].split(':');
    if(he.length === 2) {
      var hk = he[0].trim();
      var hv = he[1].trim();
      headers[hk.toLowerCase()] = hv;
    }
  }
  return headers;
};

Fns.prototype.remAttr = function attrRem(el, attr, delay) {
	if(el) {
		setTimeout(function() {
			el.removeAttribute(attr || '');
		}, parseInt(delay, 10) > 0 ? delay : 0);
	}
};

Fns.prototype.getCertInfo = function getCertInfo(id) {
	var url, el, cview;
	if(id === 'pem_cert') {
		url = '/peminfo';
	} else if(id === 'live_cert') {
		url = '/certinfo';
	}

	el = document.querySelector('#' + id);
	cview = document.querySelector('#cview');

	var self = this;
	this.ajax({'url': url}, function(err, data, done) {
		el.style.cssText = 'border-color: blue; border-style: solid;';
		if(done) {
		  self.updateUserInfo('#user_info');
			el.innerHTML = data;
			if(err) {
				el.className = 'certinfo cert_err';
			} else {
				var prod = data.match(/Let's Encrypt Authority X3/);
				el.className = prod ? 'certinfo cert_prod' : 'certinfo cert_test';
				if(id === 'pem_cert') { //only set cview if is pem certinfo
					cview.setAttribute('data-ca-type', prod ? 'prod' : 'test');
					if(prod) self.elSel(cview);
				}
			}
			self.remAttr(el, 'style', 100);
		}
	});
};

Fns.prototype.createMsg = function createMsg(html, type, bfel, time) {
  var msgel = document.createElement('div');
  msgel.className = 'popup ' + type;
  msgel.innerHTML = html;
  if(bfel) {
    var created = bfel.parentNode.insertBefore(msgel, bfel.nextSibling);
    var closable = !created.className.match(/no_close/);
    created.title = closable ? 'Zavřít zprávu' : 'Tuto zprávu nelze zavřít';
    created.onclick = function() {
      if(created && closable) {
        created.parentNode.removeChild(created);
      }
    };
    if(typeof time === 'number' && time > 0) {
      setTimeout(function() {
        if(created && closable) {
          created.parentNode.removeChild(created);
        }
      }, time);
    }
  }
};

Fns.prototype.runUrlLog = function runUrlLog(url, cgen, cb) {
	cgen.setAttribute('data-hidden',  'false');
  var self = this;
  
	this.ajax({'url': url}, function(err, data, done) {
		cgen.textContent = '';
		cgen.setAttribute('data-hidden', 'false');
		cgen.style.borderColor = 'blue';
		cgen.innerHTML = data;
		
		if(done) { //run callvack
		  self.updateUserInfo('#user_info');
			if(err || (data && data.match(/\[error\]/))) {
  			cgen.style.borderColor = 'red';
  			self.createMsg('V logu se objevily nějaké chyby!', 'msg_err msg_log', cgen, 4000);
  		} else {
  			self.remAttr(cgen, 'style', 100);
  			self.createMsg('Všecko dobře dopadlo, log neobsahuje chyby.', 'msg_ok_blue msg_log', cgen, 4000);
  		}
  		
  		if(typeof cb === 'function') {
				cb();
			}
		}
		cgen.scrollTop = cgen.scrollHeight;
	});
};

Fns.prototype.runCertGen = function runCertGen(url, type, cgen) {
	if(window.confirm('Nyní bude zpuštěno generování ' + (type === 'production' ? 'OSTRÉ' : 'TESTOVACÍ') + ' verze SSL certifikátu')) {
		var self = this;
		this.runUrlLog(url + '?catype=' + type, cgen, function() {
			self.getCertInfo('pem_cert');
		});
	}
};

Fns.prototype.runZipGen = function runZipGen(url, cgen, cb) {
	this.runUrlLog(url, cgen, cb);
};

Fns.prototype.runInstall = function runinstall(url, cgen, cb) {
	if(window.confirm('Nyní bude zpuštěna instalace certifikátů na hosting wedos')) {
		this.runUrlLog(url, cgen, cb);
	}
};

Fns.prototype.viewKeys = function viewKeys(el, key_type, cview) {
	var bid = el.id; //button #id
	var key_type_map = {
		pkey: {name: 'Privátníklíč PEM', type: 'private'},
		cert: {name: 'Certifikát PEM', type: 'cert'},
		chkey: {name: 'Certifikační řetěz PEM', type: 'chain'},
	};

	var url = '/getkey?ktype=' + key_type_map[bid].type;
	
	var self = this;
	this.ajax({'url': url}, function(err, data, done) {
		cview.style.borderColor = 'blue';
		cview.setAttribute('data-view-key', bid);
		cview.setAttribute('data-hidden', 'true');
		
		cview.innerHTML = data;
		if(done) {
		  self.updateUserInfo('#user_info');
			cview.innerHTML = data;
			key_type.textContent = err ? key_type_map[bid].name + ' nebyl načten!' : key_type_map[bid].name;
			self.activeKeyView(cview, key_type);
			self.remAttr(cview, 'style', 100);
		}
	});
};

Fns.prototype.activeKeyView = function activeKeyView(el, key_type) {
	var bt = document.querySelector('#' + el.getAttribute('data-view-key'));
	if(bt) {
		var cuact = bt.getAttribute('data-active');
		if(cuact === 'true') {
			bt.setAttribute('data-active', 'false');
			this.remAttr(el, 'data-view-key', 100);
			el.setAttribute('data-hidden', 'true');
			key_type.textContent = 'Vyberte klíč k zobrazení';
		} else if (!cuact || cuact === 'false') {
			var oldact = document.querySelector('[data-active="true"]');
			if(oldact) {
				oldact.setAttribute('data-active', 'false');
			}
		  bt.setAttribute('data-active', 'true');
		  el.setAttribute('data-hidden', 'false');
		  this.elSel(el);
		}
	}
};

//auto selection key/certificate content
Fns.prototype.elSel = function elSel(el) {
	var selection = document.getSelection();
  var range = document.createRange();
  
  var ktype = el.getAttribute('data-ca-type');
  if(ktype === 'prod') {
  	range.selectNodeContents(el); //add text content
  	selection.removeAllRanges(); //delete
  	selection.addRange(range); //ctrl+a -> selection
  }
};

//escape html
Fns.prototype.esc = function esc(text) {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };
  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
};

Fns.prototype.addPopRemove = function addPopRemove(cssquery) {
  var pops = document.querySelectorAll(cssquery);
  if(pops) {
    for(var i = 0; i < pops.length; i++) {
      var p = pops[i];
      p.onclick = function closeMsg() {
        if(!this.className.match(/no_close/)) {
          if(this.className.match(/conf_desc/)) {
            this.removeAttribute('style');
          } else {
            this.parentNode.removeChild(this);
          }
        }
      };
    }
  }
};

Fns.prototype.updateUserInfo = function updateUserInfo(cssquery) {
  var uel = document.querySelector(cssquery);
  if(uel) {
  	var self = this;
    this.ajax({url: '/logged/as'}, function(err, res, done) {
      uel.style.borderColor = 'red';
      if(done) {
      	self.remAttr(uel, 'style', 100);
        if(err) {
          uel.className = 'popup msg_err';
          uel.innerHTML = 'Nepodařilo se aktualizovat info o přihlášeném uživateli. Chyba: ' + err;
        } else if(res) {
          uel.className = 'popup msg_ok_green';
          uel.innerHTML = res;
        }
      }
    });
  }
};
