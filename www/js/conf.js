window.onload = function() {
  /* globals Fns */
	"use strict";
	function viewConfHelp(e) {
		var ckey = e.target.id.replace(/he_/, '');
		if(ckey) {
		  var desc = document.querySelector('#desc_' + ckey);
			var ke = document.querySelector('#he_' + ckey);
			var row = document.querySelector('#row_' + ckey);
			if(ke && row) {
			  var rrect = row.getClientRects(); 
		    var x = rrect.left;
		    var y = rrect.top;
        var keh = ke.clientHeight;
		    console.log('ROW left', x, 'ROW top', y, 'H3 height', keh);
		    desc.style.cssText = 'position:absolute;display:table-cell;';
		    desc.style.left = x + 'px';
		    desc.style.top = y + keh + 'px';
		  }
		}
	}
	
	function addMulti(e) {
		var mel = e.target.parentNode;
		var nadd = mel.cloneNode(true);
		var ad = mel.parentNode.insertBefore(nadd, mel.nextSibling);
		ad.childNodes[1].value = '';
		ad.childNodes[1].focus();
		addEvents();
	}
	
	function remMulti(e) {
		var mel = e.target.parentNode;
		var nrem = mel.parentNode;
	  
	  //number of multiinput elements
	  var mi = nrem.querySelectorAll('.conf_multi_input');
	  if(mi.length > 1) {
		  nrem.removeChild(mel);
	    addEvents();
	  } else {
	    e.target.nextElementSibling.value = null;
	  }
	}
	
	function addEvents() {
		var add_mults = document.querySelectorAll('.conf_multiadd_plus');
		var rem_mults = document.querySelectorAll('.conf_multiadd_minus');
		var focus_help = document.querySelectorAll('.conf_key');
		
		for(var i = 0; i < focus_help.length; i++) {
			var el = focus_help[i];
			el.onclick = viewConfHelp;
			//el.onblur = hideConfHelp;
		}
		
		for(var j = 0; j < add_mults.length; j++) {
			var plbt = add_mults[j];
			plbt.onmousedown = addMulti;
		}
		
		for(var k = 0; k < rem_mults.length; k++) {
			var mibt = rem_mults[k];
			mibt.onmousedown = remMulti;
		}
	}
	
	addEvents();
	var fns = new Fns();
	fns.addPopRemove('.popup');
};