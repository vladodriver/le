/* globals Fns */
window.onload = function() {
	"use strict";
	var livecert = document.querySelector('#live_cert'); //refresh domain ssl cert info
	var gencert = document.querySelector('#pem_cert'); //refresh local pem cert info
	
	var pkey = document.querySelector('#pkey'); //view private
	var cert = document.querySelector('#cert'); //view cert
	var chkey = document.querySelector('#chkey'); //view cert chain
	
	var key_type = document.querySelector('#key_type'); //display type of key
	var cview = document.querySelector('#cview'); //for view keys
	var cgen = document.querySelector('#cgen'); //log generation certificates
	
	var tb = document.querySelector('#test');  //test CA
	var pb = document.querySelector('#prod');  //production CA
	var zip = document.querySelector('#zipkeys'); //zip generated files and download
	var inst = document.querySelector('#install'); //zip generated files and download
	var zipdown = document.querySelector('#zipdown'); //generate zip and redirect to download
	var log = document.querySelector('#showlog'); //hide /show log
	
	var fns = new Fns();

	//generation LE certificate
	zip.onclick = function() { fns.runZipGen('/archive', cgen); }; //cert keys zip file archive create
	zipdown.onclick = function() { fns.runZipGen('/archive', cgen, function() {location.href = '/getzip';});};
	inst.onclick = function() { fns.runInstall('/install?validate=yes&renew=no&install=yes', cgen); }; //cert keys zip file archive create
	tb.onclick = function() { fns.runCertGen('/run', 'testing', cgen); }; //generate test cert
	pb.onclick = function() { fns.runCertGen('/run', 'production', cgen); }; //generate prod. cert
	
	//private, certificate and chain
	pkey.onclick = cert.onclick = chkey.onclick = function showKey() {
		fns.viewKeys(this, key_type, cview);
	};
	
	cview.onclick = function() {
		fns.activeKeyView(this, key_type);
	};
	
	livecert.onclick = gencert.onclick = function refresh() {
		fns.getCertInfo(this.id);
	};
	
	cgen.onclick = log.onclick = function gen() {
		var logopen = cgen.getAttribute('data-hidden') === 'false';
		cgen.setAttribute('data-hidden', logopen ? 'true' : 'false');
		
		//close log msgs, when closing log
		var msgs = document.querySelectorAll('.msg_log');
		if(msgs) {
		  for(var i = 0; i < msgs.length; i++) {
		    msgs[i].onclick();
		  }
		}
		if(logopen) {
			cgen.scrollTop = cgen.scrollHeight;
		}
	};
	
	fns.addPopRemove('.popup');
};