window.onload = function() {
	/* globals Fns */
	"use strict";
	var fns = new Fns();
  
  function updateUserList(el) {
    var table = el.querySelector('#user_list');
    table.style.borderColor = 'red';
    
    fns.ajax({url: '/user/list?ajax=yes'}, function(err, data, done) {
      if(done) {
        if(err) {
          el.innerHTML = '<div class="popup msg_err">Chyba načtení seznamu uživatelů ze sítě: ' +
            err + '<div>';
        } else {
         el.innerHTML = data; 
        }
        fns.remAttr(table, 'style', 200);
        setTimeout(function() { table.removeAttribute('style'); }, 200);
        addListEvents();
      }
    });
  }
  
  //err, res, msgel, msgstart, msgclass, tim
  function userApiCommand(action, params, apiurl, afterel) {
    //params = Array.isArray(params) ? params : [];
    params = (params && typeof params === 'object' && typeof params.length !== 'undefined') ? params : [];
    var json = JSON.stringify({action: action, params: params});
    var metmap = {
      updateUser: 'put',
      createUser: 'post',
      removeUser: 'delete'
    };
    
    var method = typeof metmap[action] !== 'undefined' ? metmap[action] : 'post';
    fns.ajax({url: apiurl, method: method, json: true, data: json}, function(err, res, done) {
      if(done) {
        createMsg(err, res, afterel);
      }
      fns.updateUserInfo('#user_info');
    });
  }
  
  function userUpdate(el, apiurl, afterel) {
    if(!el.getAttribute('disabled')) {
      var user = el.getAttribute('data-for-user');
      var elpass = document.querySelector('.edit_passwd[data-for-user="' + user + '"]');
      var eladm = document.querySelector('.edit_admin[data-for-user="' + user + '"]');
      
      if(user && elpass && eladm) {
        
        var pass = elpass.value;
        var adm = eladm.checked;
        userApiCommand('updateUser', [user, pass, adm], apiurl, afterel);
      }
    }
  }
  
  function userRemove(el, apiurl, afterel) {
    if(!el.getAttribute('disabled')) {
      var user = el.getAttribute('data-for-user');
      if(user) {
        userApiCommand('removeUser', [user], apiurl, afterel);
      }
    }
  }
  
  function userAdd(el, apiurl, afterel) {
    if(!el.getAttribute('disabled')) {
      var user = document.querySelector('#add_username');
      var pass = document.querySelector('#add_passwd');
      var adm = document.querySelector('#add_admin');
      if(user) {
        userApiCommand('createUser', [user.value, pass.value, adm.checked], apiurl, afterel);
      }
    }
  }

  function createMsg(err, res, afterel) {
    var data, error;
    if(err && !res) {
      error = 'Chyba dotazu na User Api: "' + err + '"';
    } else {
      try {
        data = JSON.parse(res);
        if(data.call.err) {
          error = data.call.msg;
        } else if (data.result.err) {
          error = data.result.msg;
        }
      } catch(e) {
        error = 'JSON parse failed: "' + e + '" !';
      }
    }
    fns.createMsg(error ? error : data.result.msg, error ? 'msg_err' : 'msg_ok_blue', afterel, 4000);
    updateUserList(afterel);
  }
  
  function addListEvents() {
    var sbtns = document.querySelectorAll('.edit_save'); //save buttons
    var rbtns = document.querySelectorAll('.edit_remove'); //delete buttons
    var apiurl = '/user/crud';
    var profiles = document.querySelector('#user_profiles'); //profiles div
    var addus = document.querySelector('#add_user');
    
    
    //save butons
    if(sbtns) {
      for(var i = 0; i < sbtns.length; i++) {
        sbtns[i].onclick = function() {
          userUpdate(this, apiurl, profiles);
        };
      }
    }
    
    //remove buttons
    if(rbtns) {
      for(var j = 0; j < rbtns.length; j++) {
        rbtns[j].onclick = function() {
          userRemove(this, apiurl, profiles);
        };
      }
    }
    if(addus) {
      addus.onclick = function() { userAdd(this, apiurl, profiles); };
    }
    fns.addPopRemove('.popup'); //remove for static popups
  }
  
  addListEvents();
};