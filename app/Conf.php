<?php
class Conf {
	private $cnj; //JSON config file path
	private $ckeys; //ckeys from spec class
	private $LC; //start base conf array
	private $BC; //end base conf array
	private $C;   //FULL preparsed conf
	private $firstline;//<?php line before json content (for security)
	
	//for log change from start load to end (__destruct)
	private $changes; //change log and error log
	
	public function __construct(ConfSpec $spec) {
		$this->changes = ['errors' => array(), 'changes' => array()];
		$this->cnj = __DIR__ . '/le-conf.php';
		$this->ckeys = $spec->ckeys;
		$this->firstline = '<?php echo "Access denied, this file is TOP SECRET (:";exit();?>' . "\n";
		
		$this->parseBaseConf();
	}
	
	private function rollBack($key, $load) {
	  //load json OR not in LC -> use ckeys default
	  $val = null;
	  if($load) {
			if(isset($this->ckeys[$key], $this->ckeys[$key]['default'])) {
			  $val = $this->ckeys[$key]['default'];
			}
		} else { //update && save -> use from LC
			if(isset($this->LC[$key])) {
			  $val = $this->LC[$key];
			}
		}
		return $val;
	}
	
	private function invalidKey($key, $val) {
		$invalid = false;
		if(isset($this->ckeys[$key])) {
			$spec = $this->ckeys[$key];
			$reg = $spec['reg'];
			$require = $spec['require'];
			$default = $spec['default'];

			if($val && ($reg && !preg_match($reg, $val))) {
				//non empty value && reg validation and not match
				$invalid = ['val' => $val, 'msg' => 'hodnota nemá správný formát'];
			} else if (!$val && $require) {
				$invalid = ['val' => $val, 'msg' => 'každý povinný klíč musí mít hodnotu, v případě pole všechny hodnoty'];
			}
		} else {
			$invalid = ['val' => $val, 'msg' => 'není platným klíčem pro konfigurační soubor'];
		}
		return $invalid;
	}
	
	private function parseConfKey($key, $val, $load) {
		$spec = $this->ckeys;
		$change = false;
		$err = false;
		
		if(isset($this->ckeys[$key])) {
			$ktype = gettype($spec[$key]['default']);
			$valtype = gettype($val);
			$oldval = $this->rollBack($key, $load);
			
			if($ktype === $valtype) {
				if($val !== $oldval) { //only if update
					$isar = is_array($val);
					$to_parse = $isar ? $val : [$val];
					
					if($isar) $val = array_unique($val); //filter dupicit values
					foreach($to_parse as $idx => $v) {
						$err = $this->invalidKey($key, $v);
					}
					$change = $err ? false : ($load ? false : ['from' => $oldval, 'to' => $val]);
					//$change = $err ? false : ['from' => $oldval, 'to' => $val];
				}
			} else { //no change
				$err = ['val' => $val, 'msg' => 'má mít datový typ: <b>' . $ktype .  '</b> a ne: <b>' . $valtype . '</b>'];
			}	
		} else { //no change
			$err = ['val' => $val, 'msg' => ' není platným klíčem pro konfigurační soubor'];
		}
		
		$res = ['err' => $err, 'key' => $key, 'change' => $change];
		return $res;
	}
	
	private function parseBaseConf() {
	  if(isset($this->LC)) {
	    $load = false;
	    $bc = $this->BC;
	  } else {
	    $load = true;
	    $this->LC = $this->loadConf();
	    $bc = $this->LC;
	  }
	  
	  $load = ($bc === $this->LC);
		$parsed_conf = [];
		foreach($this->ckeys as $key => $kspec) {
			if(isset($bc[$key])) {
				$val = $bc[$key];
				$res = $this->parseConfKey($key, $val, $load); //get changed value and err
			} else { //not found change to default or LC
				$res = ['err' => false, 'key' => $key, 'change' => ['to' => $this->rollBack($key, $load)] ];
			}
			
			if($res['err']) {
				$parsed_conf[$key] = $this->rollBack($key, $load); //reset because error
				unset($res['change']);
				array_push($this->changes['errors'], $res); //log to errors
			} else if($res['change']) {
				$parsed_conf[$key] = $res['change']['to']; //update key
				unset($res['err']);
				array_push($this->changes['changes'], $res); //log to changes
			} else if(!$res['err'] && !$res['change']) {
				$parsed_conf[$key] = $val; //no change -> push $val
			}
		}
    
		$this->BC = $parsed_conf;
		$this->makeFullConf();
	}
	
	private function loadConf() {
		if(file_exists($this->cnj)) {
		  $fcon = file_get_contents($this->cnj); //file content
			$json = preg_split('~\n~', $fcon)[1];
			$conf = json_decode($json, true);
		} else {
			$conf = [];
		}
		return $conf;
	}
	
	//make php array for all options rom $this->C
	private function makeFullConf() {
		$this->C = $this->BC;
		$this->C['sslUrl'] = 'https://' . $this->C['domain'];
		$this->C['webRoot'] = $_SERVER['DOCUMENT_ROOT'];
		$this->C['hostRoot'] = realpath($this->C['webRoot'] . '/../');
		$this->C['lePath'] = $this->C['hostRoot'] . '/' . $this->C['lePath'];
		
		//if changed lePath value - rename it and if lePath dir not exists create it
		//rename if old path exists in loaded config file
		if(!file_exists($this->C['lePath']) && isset($this->LC['lePath'])) {
		  $oldpath = $this->C['hostRoot'] . '/' . $this->LC['lePath'];
			if(isset($this->LC['lePath']) && file_exists($oldpath)) {
				rename($oldpath, $this->C['lePath']);
			} else {
				mkdir($this->C['lePath'], 0777, true); //create new
			}
		}
		$this->C['certPath'] = $this->C['lePath'] . '/' . $this->C['domain'];
		
		//set domain content domainDocDir
		$setRootDir = ($this->C['domainRootDir']);
		$this->C['domainRootDir'] = $setRootDir ? '/' . $this->C['domainRootDir'] . '/' : '/';
		
		$this->C['domainDocDir'] = $setRootDir ?
			$this->C['domainRootDir'] . $this->C['domain'] . '/' . $this->C['domainDocDir']:
			$this->C['domainRootDir'];
		
		//make alternative dns names for certificate request
		$this->C['dns'] = array();
		//make main domain and www.domain alt subject name
		foreach(array($this->C['domain'], 'www.' . $this->C['domain']) as $dom) {
			array_push($this->C['dns'], array($dom, $this->C['domainDocDir']));
		}
		
		//make subdomains alt subject names if any found
		foreach($this->C['subnames'] as $subname) {
			$subDomainDocRoot = '/' . $this->C['subDomainRootDir'] . '/' . $subname . '/' . $this->C['subDomainDocDir'];
			array_push($this->C['dns'], array($subname . '.' . $this->C['domain'], $subDomainDocRoot));
		}
		
		//recalculate days for tmlBefore and tmlAfter
		$this->C['tmlAfter'] = $this->C['tmlAfter'] * 86400;
		$this->C['tmlBefore'] = $this->C['tmlBefore'] * 86400;
		
		// Static values for Lescript and valid LE ca name
		$this->C['prodCaUrl'] = 'https://acme-v01.api.letsencrypt.org'; 
		$this->C['testCaUrl'] = 'https://acme-staging.api.letsencrypt.org';
		$this->C['prodCaName'] = "Let's Encrypt Authority X3";
		//make full path tp zip file and users file
		
		$this->C['zipFile'] = $this->C['lePath'] . '/' . $this->C['zipFile'];
		$this->C['passwdFile'] = $this->C['lePath'] . '/' . $this->C['passwdFile'];
		
		//if change users filename or zip filename, rename it
		foreach(['zipFile', 'passwdFile'] as $fkey) {
			$oldfile = isset($this->LC[$fkey]) ? $this->C['lePath'] . '/' . $this->LC[$fkey] : false;
			if($oldfile) {
			  $newfile = $this->C[$fkey];
			  if(!file_exists($newfile) && (is_file($oldfile) && file_exists($oldfile))) {
				rename($oldfile, $newfile);
			  }
			}
		}
	}
	
	private function formToVal($key, $formval) {
		if(isset($this->ckeys[$key])) {
			$type = gettype($this->ckeys[$key]['default']);
			if($type === 'integer') {
				$val = intval($formval); //convert to integer
			} else if($type === 'boolean') {
				$val = $formval === 'false' ? false : true; //convert to boolean
			} else if($type === 'array') {
				if(sizeof($formval) === 1 && !$formval[0]) {
					$val = []; //one empty element -> reset to empty array
				} else {
					$val = $formval;
				}
			} else { //other no need conversion
				$val = $formval;
			}
		} else {
			$val = null;
		}
		return $val;
	}
	
	public function getAll() {
		return ['log' => $this->changes, 'lc' => $this->LC, 'bc' => $this->BC,
			'c' => $this->C, 'ckeys' => $this->ckeys]; 
	}
	
	public function setAll(array $cfg, $form = false) {
		foreach($cfg as $key => $val) {
			if($form) {
				$this->BC[$key] = $this->formToVal($key, $val);
			} else {
				$this->BC[$key] = $val;	
			}
		}
		$this->parseBaseConf(); //reparse all
	}
	
	public function __get($key) {
		if(isset($this->C[$key])) {
			return $this->C[$key];
		} else {
			return null;
		}
	}
	
	public function __set($key, $val) {
		if(isset($this->ckeys[$key])) {
			$this->BC[$key] = $val;
			$this->parseBaseConf(); //reparse single
		}
	}
	
	//called only from __desctuct
	private function saveConf($json) {
		file_put_contents($this->cnj, $this->firstline . $json);
	}
	
	public function __destruct() {
		$start = json_encode($this->LC, JSON_UNESCAPED_UNICODE);
		$end = json_encode($this->BC, JSON_UNESCAPED_UNICODE);

		if($start !== $end) {
			$this->saveConf($end);
		}
	}
	
}