<?php
class Res {
  //private $cres; //raw curl response
  
  public $http;
	public $code;
	public $msg;
	public $err;
	public $headers;
	public $body;
	
	public function __construct($err, $code, $res, $hsize) {
		$this->err = $err; //curl error
		$this->code = $code; //http CODE
		$this->parseResponse($res, $hsize);
	}
	
	private function parseResponse($cres, $hsize) {
	  //cut response to HEAD and BODY
	  if($cres) {
			$res_head = substr($cres, 0, $hsize); //http HEAD part
			$this->body = substr($cres, $hsize); //http BODY part
		} else {
			$res_head = null;
			$this->body = null;
		}
		
	  if($res_head) { //parse HTTP message only
	    $reg = '~^HTTP\/(\d\.\d) ' . $this->code . ' (.*)$~m';
	    $rgm = [];
	    preg_match($reg, $res_head, $rgm);
	    if(isset($rgm[1])) {
	      $ver = $rgm[1];
	    }
	    if(isset($rgm[2])) {
	      $msg = $rgm[2];
	    }
	    $this->msg = $msg;
	    $this->http = $ver;
	    $this->parseHead($res_head);
	  } else {
	    $this->msg = null;
	    $this->http = null;
	    $this->headers = null;
	  }
	}
	
	private function parseHead($headstr) {
		$lines = explode("\n", $headstr);
		$headers = array();
		$cookies = array();
		
		foreach($lines as $ln) {
		  $hreg = '~^(.+?): (.+)$~'; //header key value regexp
		  $hmatch = [];
		  
		  if(preg_match($hreg, $ln, $hmatch)) {
		    if(strtolower($hmatch[1]) === 'set-cookie')
		    $cookies[$hmatch[1]] = $hmatch[2];
		    $headers[$hmatch[1]] = $hmatch[2];
		  }
		}
		$this->cookies = $cookies;
		$this->headers = $headers;
	}
	
	public function find($patern, $idx, $decode = true) {
		if($this->body) {
			$res = array();
			$m = preg_match($patern, $this->body, $res);
			
			if($m && array_key_exists($idx, $res)) {
				$f = $decode ? htmlspecialchars_decode($res[$idx]) : $res[$idx];
			} else {
				$f = null;
			}
		} else {
			$f = null;
		}
		return $f;
	}
}
