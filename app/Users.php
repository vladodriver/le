<?php
class Users {
	private $passfile;
	private $nlen;
	private $plen;
	private $sesslen;
	
	public $usersCreated;
	
	public function __construct($sesslen, $secure, $passfile, $nlen, $plen) {
		$this->sesslen = $sesslen; //session max inactivity
		$this->secure = $secure; //session require cookie over SSL
		$this->passfile = $passfile;
		$this->nlen = $nlen; //minimal username length
		$this->plen = $plen; //minimal password length
		
		$this->usersCreated = $this->usersExists(); //check number of users created
		
		//starting session
		if(isset($_COOKIE, $_COOKIE['le-sid'])) {
		  $this->sessionStart($this->sesslen, '/', false, $this->secure, true);
		}
	}
	
	public function __call($action, $args) {
		$cu = $this->CU();
		if(method_exists($this, $action)) {
			$CA = $this->getCallAction($action, $args, $cu);
		} else {
			$CA = ['err' => new Exception('Action method: \'' . $action . '\' not method in Users class'),
				'msg' => 'Metoda akce třídy Users: \'' . $action . '\' není implementována.', 'code' => 500];
		}
		$CA['action'] = $action;
		$CA['params'] = $args;
		$CA['user'] = $this->CU();
		
		$res = array();
		
		if($CA['err']) {
			$res['result'] = null;
		} else {
			$res['result'] = call_user_func_array([$this, $action], $args);
			if($res['result']['err']) {
				$res['result']['err'] = $res['result']['err'];
			}	
		}
		$res['call'] = $CA;
		return $res;
	}
	
	private function CU() {
		$cu = isset($_SESSION, $_SESSION['username']) ? $_SESSION['username'] : null;
		return $cu;
	}
	
	private function getCallAction($action, $args, $cu) {
		$uinfo = $this->getUsers($cu);
		if($uinfo['err']) {
			//get user info failed
			$call = ['err' => $uinfo['err'], 'msg' => $uinfo['msg'], 'code' => 404];
		} else if(!$cu) {
			$call = ['err' => new Exception('You must be logged in for call \'' . $action . '\''),
				'msg' => 'Pro volání akce <b>' . $action . '</b> musíte být přihlášen', 'code' => 401];
		} else {
			$R = [
				'sessionDestroy' => [],
				'getUsers' => [
					'admin' => [
						'allow' => (sizeof($args) === 0 || sizeof($args) > 0 && gettype($args[0]) === 'string'),
						'msg' =>  'Administrator může vypsat kohokoliv nebo všechny uživatele.',
						'err' => new Exception('1. argument must be string'),
						'code' => 400
					],
					'user' => [
						'allow' => (sizeof($args) > 0 && $args[0] === $cu),
						'msg' => 'Uživatel <b>' . $cu . '</b> bez administrátorských práv může vypsat jen sám sebe',
						'err' => new Exception('User \'' . $cu .'\' is not allowed to list another user except myself'),
						'code' => 401
					]
				],
				'updateUser' => [
					'admin' => [
						'allow' => (sizeof($args) > 2 && gettype($args[0]) === 'string' &&
							gettype($args[1]) === 'string' && gettype($args[2]) === 'boolean' && ($args[0] !== $cu || $args[2])),
						'msg' =>  'Administrator může editovat kteréhokoliv uživatele, ale nemůže snížit své oprávnění',
						'err' => new Exception('Admin can edit passwords and rights another\'s users, except own rights'),
						'code' => 400
					],
					'user' => [
						'allow' => (sizeof($args) > 2 && $args[0] === $cu && gettype($args[1]) === 'string' &&
							gettype($args[2]) === 'boolean' && !$args[2]),
						'msg' => 'Uživatel <b>' . $cu . '</b> bez administrátorských práv může upravit jen vlastní heslo',
						'err' => new Exception('User \'' . $cu .'\' is not allowed to edit own rights or profile another users'),
						'code' => 401
					]
				],
				'createUser' => [
					'admin' => [
						'allow' => (sizeof($args) > 2 && gettype($args[0]) === 'string' && gettype($args[1] === 'string') 
							&& gettype($args[2]) === 'boolean'),
						'msg' => 'Adminsitrátor může vytvářet nové uživatele zadáním přihlašovacího jména a hesla',
						'err' => new Exception('1. and 2. argument must be string, 3. must be boolean'),
						'code' => 400
					],
					'user' => [
						'allow' => false,
						'msg' => 'Uživatel <b>' . $cu . '</b> bez administrátorských práv nemůže zakládat nové uživatele',
						'err' => new Exception('User \'' . $cu .'\' is not allowed to create new user profiles'),
						'code' => 401
					]
				],
				'removeUser' => [
					'admin' => [
						'allow' => (sizeof($args) > 0 && gettype($args[0]) === 'string' && $args[0] !== $cu),
						'msg' => 'Adminsitrátor může odstranit kteréhokoli uživatele kromě sebe, když je přihlášen.',
						'err' => new Exception('1. argument must be string. Logged admin user cannot remove self.'),
						'code' => 401
					],
					'user' => [
						'allow' => (false),
						'msg' => 'Uživatel <b>' . $cu . '</b> nemůže odstranit uživatele, protože není admin.',
						'err' => new Exception('User \'' . $cu .'\' is not allowed to remove user profiles, because not admin'),
						'code' => 401
					]
				]
			];
			
			$isadm = $uinfo['data'][$cu]['admin']; //is admin
			if(array_key_exists($action, $R)) {
				$right = $R[$action];
				if(sizeof($right) === 0) { //allow all
					//$allow = true;
					$err = false;
					$code = 200;
					$msg = 'Akce: <b>' . $action . '</b> je povolena všem přihlášeným uživatelům.';
				} else { //parse rights
					$allow = $isadm ? $R[$action]['admin']['allow'] : $R[$action]['user']['allow'];
					//get err if not allow or false
					if($allow) {
						$err = false;
						$code = 200;
					} else {
						$err = $isadm ? $R[$action]['admin']['err'] : $R[$action]['user']['err'];
						$code = $isadm ? $R[$action]['admin']['code'] : $R[$action]['user']['code'];
					}
					//get msg
					$msg = $isadm ? $R[$action]['admin']['msg'] : $R[$action]['user']['msg'];
				}
				$call = ['err' => $err, 'msg' => $msg, 'code' => $code];
			} else {
				$call = ['err' => new Exception('Action \'' . $action . '\' is not public Users API method'),
						'msg' => 'Akce: <b>' . $action . '</b> není veřejnou metodou Users API' , 'code' => 500];
			}
		}
		return $call;
	}
	
	private function sessionStart($expiry, $path, $domain, $secure, $httponly) {
		session_set_cookie_params(0, $path, $domain, $secure, $httponly); //expiry is 0
		session_name('le-sid');
		if(session_status() === PHP_SESSION_ACTIVE) {
		  $this->sessionDestroy();
		} else {
		  session_start();
		}
	  
		
		//expirated session destroy..
		if (isset($_SESSION['LAST_ACTIVITY'], $_SESSION['EXPIRE']) &&
			(time() - $_SESSION['LAST_ACTIVITY'] > $expiry)) {
    	$this->sessionDestroy();
		} else { //non expirated are refreshed
			$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
		}
		$_SESSION['EXPIRE'] = (time() + $expiry);
	}
	
	//method for logout current user
	private function sessionDestroy() {
		$cu = $this->CU();
		if($cu) {
			if(isset($_SESSION)) {
				session_unset();
				session_destroy();
				unset($_SESSION);
				setcookie('le-sid', '', time() - 3600);
			}
			$res = ['err' => false, 'data' => null, 'msg' => 'Ukončení relace přihlášeného uživatele: <b>' . $cu . '</b>',
			'code' => '200'];
		} else {
			$res = ['err' => new Exception('No any user logged in'), 'data' => null, 'msg' => 'Žádný uživatel není přihlášen', 'code' => '404'];
		}
		return $res;
	}
	
	private function makeUsers(array $users) {
		$lines = array();
		foreach ($users as $name => $cred) {
			$l = $name . ':' . $cred['hash'] . ':' . (int)$cred['admin'];
			array_push($lines, $l);
		}
		$fcnt = implode("\n", $lines);
		return $fcnt;
	}
	
	//check if users found for first login
	private function usersExists() {
		$ulist = $this->getUsers();
		if($ulist['data'] && gettype($ulist['data']) === 'array' && sizeof($ulist['data']) > 0) {
			return sizeof($ulist['data']);
		} else {
			return 0;
		}
	}
	
	//public method for crete first users
	public function createFirstAdminUser($login, $passwd) {
		if(!$this->usersExists()) {
			$user = $this->createUser($login, $passwd, true);
			if(!$user['err'] && $user['code'] === 200) {
				$_SESSION['username'] = $login;
				$_SESSION['admin'] = true;
			}
		} else {
			$user = ['err' => new Exception('This is not first login. Any users already found'),
				'msg' => 'Toto není první přihlášení. Nejméně jeden uživatel je již vytvořen.' ,
				'data' => null, 'code' => '401'];
		}
		$res = array();
		$res['call'] = ['err' => false, 'msg' => 'Pokus o vytvoření prvního uživatele může volat bez autentifikace.',
		'code' => 200, 'action' => 'createFirstadminUser', 'params' => func_get_args(), 'user' => null];
		$res['result'] = $user;
		return $res;
	}
	
	//public methods for authentification
	public function authUser($login, $passwd) {
		$uinfo = $this->getUsers($login);
		if($uinfo['err']) {
			$auth = ['err' => $uinfo['err'], 'msg' => 'Získání dat o uživateli : <b>' . $login .
				'</b> selhalo', 'data' => null, 'code' => $uinfo['code']];
		} else if(!$uinfo['data']) {
			$auth = ['err' => new Exception('User not found'), 'msg' => 'Uživatel : <b>' . $login .
				'</b> neexistuje', 'data' => null, 'code' => 404];
		} else {
			$ok = password_verify($passwd, $uinfo['data'][$login]['hash']);
			if($ok) {
			  $this->sessionStart($this->sesslen, '/', false, $this->secure, true);
				session_regenerate_id(true);
				$_SESSION['username'] = $login;
				$_SESSION['admin'] = $uinfo['data'][$login]['admin'];
				$auth = ['err' => false, 'msg' => 'Vítáme přihlášeného uživatele: <b>' . $login .
					'</b>.', 'data' => $uinfo['data'], 'code' => 200];
			} else { //invalid passwd
				sleep(1);
				$auth = ['err' => new Exception('Invalid password'), 'msg' => 'Chybné heslo', 'data' => null, 'code' => 401];
			}
		}
		$res = array();
		$res['call'] = ['err' => false, 'msg' => 'Požadavek na autentifikaci.',
		'code' => 200, 'action' => 'authUser', 'params' => func_get_args(), 'user' => null];
		$res['result'] = $auth;
		return $res;
	}
	
	//CRUD auth api for auth users
	//method for list all or $only username
	private function getUsers($only = false) {
		if(file_exists($this->passfile)) {
			try {
				$fc = file_get_contents($this->passfile);
				$lines = explode("\n", $fc);
				$users = array();
				
				foreach($lines as $line) {
					$cut = explode(':', $line);
					if(sizeof($cut) === 3 && (strlen($cut[0]) > 0 && strlen($cut[1]) > 0 && strlen($cut[2]) >0)) {
						$name = $cut[0];
						$hash = $cut[1];
						$admin = $cut[2];
						//apply $only filter or list all
						if($only === false  || ($only && ($only === $name))) {
							$users[$name] = ['hash' => $hash, 'admin' => $admin];	
						}
					}
				}
				if(sizeof($users) > 0) {
					$list = ['err' => false, 'data' => $users, 'msg' => 'Data <b>' . sizeof($users) .
						'</b> uživatelů získána.', 'code' => 200];
				} else {
					$list = ['err' => false, 'data' => null,
						'msg' => 'Nenalezen žádný uživatel' . ($only ? ' <b>' . $only . '</b>.' : '.'), 'code' => 200];
				}
			} catch(Exception $e) {
				$list = ['err' => $e, 'data' => null, 'msg' => 'Seznam uživatelů se nepodařilo získat.', 'code' => 500];
			}
		} else {
			$list = ['err' => false, 'data' => null,
			'msg' => 'Soubor uživatelů ještě nebyl vytvořen. Je třeba založit prvního uživatele.', 'code' => 200];
		}
		return $list;
	}
	
	private function writeUser($name, $passwd, $admin, $update = false) {
		//check minimal password and name length
		if(gettype($name) !== 'string' || strlen($name) < $this->nlen) {
			$crt = ['err' => new Exception('Minimal name length must be >= ' . $this->nlen),
				'msg' => 'Minimální délka uživatelského jména musí být >= <b>' . $this->nlen . '</b>',
				'data' => null, 'code' => 400];
		} else if(gettype($passwd) !== 'string' || (!$update && strlen($passwd) < $this->plen) ||
		  ($update && (strlen($passwd) > 0 && strlen($passwd) < $this->plen))) {
			$crt = ['err' => new Exception('Minimal password length must be >= ' . $this->plen),
				'msg' => 'Minimální délka hesla musí být >= <b>' . $this->plen . '</b>', 'data' => null, 'code' => 400];
		} else {
			$uinfo = $this->getUsers($name);
			if($uinfo['err']) {
				$crt = ['err' => $uinfo['err'], 'msg' => $uinfo['msg'], 'data' => null, 'code' => $uinfo['code']];
			} else {
				$hash = password_hash($passwd, PASSWORD_BCRYPT);
				//username $name already exists for create action
				if(!$update && $uinfo['data'] && array_key_exists($name, $uinfo['data'])) {
					$crt = ['err' => new Exception('Create user \'' . $name . '\' failed. Username already exists'),
						'msg' => 'Vytvoření nového uživatele: <b>' . $name . '</b> selhalo. Uživatel už existuje',
						'data' => null, 'code' => 401];
				} else if($update && (!$uinfo['data'] || !array_key_exists($name, $uinfo['data']))) {
				//username $name not exists for update action
					$crt = ['err' => new Exception('Update user \'' . $name . '\' failed. Username not exists'),
						'msg' => 'Změna údajů uživatele <b>' . $name . '</b> selhala. Uživatel neexistuje',
						'data' => null, 'code' => 404];
				} else {
					$list = $this->getUsers();
					//get users list or create new if is user list is null
					if(!$list['data']) {
						$uar = array(); //create new user list
					} else {
						$uar = $list['data']; //use existing
					}
					
					//only update admin rights for existing user -> use existing hash
					$need_write = true;
					if($update && !$passwd) {
					  $uar[$name] = array();
					  $uar[$name]['hash'] = $passwd ? password_hash($passwd, PASSWORD_BCRYPT) : $uinfo['data'][$name]['hash'];
					  if(boolval($admin) !== boolval($uinfo['data'][$name]['admin'])) {
					    $uar[$name]['admin'] = is_bool($admin) ? $admin : $uinfo['data'][$name]['admin'];
					    $umsg = ' byl úspěšně upraven.';
					  } else {
					    $need_write = false; //no need save users db
					  } 
					} else { //create new user
					  $uar[$name] = ['hash' => password_hash($passwd, PASSWORD_BCRYPT), 'admin' => $admin];  
					}
					
					if($need_write) {
					$fcnt = $this->makeUsers($uar);
  					$cruser = array(); $cruser[$name] = $uar[$name]; //for response about created or updated user changes
  					//make file content
  					try {
  						file_put_contents($this->passfile, $fcnt);
  						$msg = ($update ? ' byl úspěšně upraven.' : ' byl úspěšně vytvořen.');
  						$crt = ['err' => false, 'msg' => 'Uživatel <b>' . $name . '</b> ' . $msg, 'data' => $cruser, 'code' => 200];
  					} catch(Exception $e) {
  						$crt = ['err' => new Exception('Unable to save data for user \'' . $name . '\'. Error: \'' . $e->getMessage() . '\''),
  							'msg' => 'Nepodařilo se uložit změnu dat pro uživatele: <b>' . $name . '</b>', 'data' => null, 'code' => 500];
  					}
					} else {
					  $crt = ['err' => new Exception('User data not any change'),
					    'msg' => 'Uživatel <b>' . $name . '</b> nebyl nijak změněn' , 'data' => null, 'code' => 200];
					}
				}
			}
		}
		return $crt;
	}
	
	private function updateUser($name, $passwd, $admin) {
		return $this->writeUser($name, $passwd, $admin, true);
	}
	
	private function createUser($name, $passwd, $admin) {
		return $this->writeUser($name, $passwd, $admin, false);
	}
	
	private function removeUser($name) {
		$uinfo = $this->getUsers($name);//get info about deleted user
		if($uinfo['err']) {
			$rem = ['err' => $uinfo['err'], 'msg' => 'Nelze získat data o uživateli: <b>' . $name . '</b>', 'code' => 500];
		} else if(!$uinfo['data']) {
			$rem = ['err' => new Exception('Removed user \'' . $name . '\' not exist'),
				'msg' => 'Uživatel: <b>' . $name . '</b> neexistuje, nelze ho tedy smazat.', 'data' => null, 'code' => 404];
		} else {
			$ulist = $this->getUsers()['data']; //get list of user
			$rmusr = array();
			$rmusr[$name] = $ulist[$name]; //for response info about removed user
			unset($ulist[$name]); //delete user from $list
			$fcnt = $this->makeUsers($ulist);
			//make new users file
			try {
				if(strlen($fcnt) === 0) {
					unlink($this->passfile); //delete users file if $fcnt is empty
				} else {
					file_put_contents($this->passfile, $fcnt); //save udated $fcnt to file
				}
				$rem = ['err' => false, 'msg' => 'Uživatel: <b>' . $name . '</b> byl úspěšně smazán', 'data' => $rmusr, 'code' => 200];
			} catch(Exception $e) {
				$rem = ['err' => $e, 'msg' => 'Nepodařilo se aktualizovat data po odstranění uživatele: <b>' . $name . '</b>',
					'data' => null, 'code' => 500];
			}
		}
		return $rem;
	}
}
