<?php
class Model {
	public $Cfg; //configuration getter/setter
	public $log; //logger
	public $U; //user managment
	
	public function __construct(Conf $cfg, Logger $loger, Users $users, Cinstall $cinst) {
		$this->Cfg = $cfg;
		$this->log = $loger;
		$this->U = $users;
		$this->cinst = $cinst;
	}
	
	public function addCertStatus(&$info, $tmlafter, $tmlbefore, $prodca, $type) {
		$info['status'] = array();
		$info['status']['role'] = $type;
		$info['status']['tmlAfter'] = $tmlafter;
		$info['status']['tmlBefore'] = $tmlbefore;
		
		if($info['err'] || !$info['data']) {
			$info['status']['canInstall'] = false;
			$info['status']['needRegen'] = true;
			$info['status']['isProduction'] = false;
		} else {
			$start = $info['data']['validFrom_time_t'];
			$end = $info['data']['validTo_time_t']; $now = date('U');
			$info['status']['canInstall'] = (($now - $start) <= $tmlafter) ? true : false;
			$info['status']['needRegen'] = (($end - $now) <= $tmlbefore) ? true : false;
			$info['status']['isProduction'] = ($info['data']['issuer']['CN'] === $prodca) ? true : false;
		}
	}
	
	public function getCertInfo() {
		$url = $this->Cfg->sslUrl;
		$tmlafter = $this->Cfg->tmlAfter;
		$tmlbefore = $this->Cfg->tmlBefore;
		$prodca = $this->Cfg->prodCaName;
		try {
			$orignal_parse = parse_url($url, PHP_URL_HOST);
			$get = stream_context_create(["ssl" => ['capture_peer_cert' => true, 'verify_peer' => false, 'verify_peer_name' => false]]);
			$read = stream_socket_client("ssl://" . $orignal_parse . ":443", $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $get);
			$cert = stream_context_get_params($read);
			$certinfo = openssl_x509_parse($cert['options']['ssl']['peer_certificate']);
			$res = ['err' => null, 'data' => $certinfo];
		} catch(Exception $e) {
			$res = ['err' => $e, 'data' => null];
		}
		$this->addCertStatus($res, $tmlafter, $tmlbefore, $prodca, 'live');
		return $res;
	}
	
	public function getPemInfo() {
		$file = $this->Cfg->certPath . '/cert.pem';
		$tmlafter = $this->Cfg->tmlAfter;
		$tmlbefore = $this->Cfg->tmlBefore;
		$prodca = $this->Cfg->prodCaName;
		try {
			$read = file_get_contents($file);
			$info = openssl_x509_parse($read);
			if($info) {
				$res = ['err' => null, 'data' => $info];
			} else {
				$res = ['err' => new Exception('Parse pem certificate failed. Return empty info'), 'data' => null];
			}
		} catch (Exception $e) {
			$res = ['err' => $e, 'data' => null];
		}
		$this->addCertStatus($res, $tmlafter, $tmlbefore, $prodca, 'pem');
		return $res;
	}

	private function recDirOp($dir, $cb) {
		if(file_exists($dir)) {
			try {
				//all flies in directory
				$files = new RecursiveIteratorIterator(
					new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS),
					RecursiveIteratorIterator::CHILD_FIRST //first files and after dirs
				);
				//run callback each file
				foreach ($files as $name => $file) {
				  $cb(false, $name, $file);
				}
			} catch (Exception $e) {
				$cb($e, null, null);
			}
		}
	}
	
	private function rmRf($dir) {
		$logger = $this->log;
		
		if(file_exists($dir)) {
			$this->recDirOp($dir, function($err, $name, $file) use (&$logger) {
				if($err) {
					$logger->error('Error accessing file or directory: "' . $err->getMessage() . '".');
				} else {
					try {
						$file->isDir() ? rmdir($name) : unlink($name);
						$logger->info(($file->isDir() ? 'Directory "' : 'File "') . $name . '" has ben deleted as unnecessary.');
					} catch(Exception $e) {
						$logger->error('Deleting file or directory failed. Error: "' . $e->getMessage() . '".');
					}
				}
			});
			
			//try root directory delete
			try {
				rmdir($dir);
				$this->log->info('Root directory "' . $dir . '" has ben deleted as unnecessary.');
			} catch(Exception $e) {
				$this->log->error('Deleting directory failed. Error: "' . $e->getMessage() . '".');
			}
		}
	}
	
	private function isEmptyDir($dir) {
		$di = new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS);
    return iterator_count($di) === 0;
	}
	
	public function dirZip() {
		$dir = $this->Cfg->certPath;
		$zfile = $this->Cfg->zipFile;
		try {
			//check if dir is empty
			if($this->isEmptyDir($dir)) {
				return $this->log->error('Directory "' . $dir . '" is empty. No files for add to archive.');
			}
		} catch(Exception $e) {
			return $this->log->error('Error access directory: ' . $e->getMessage());
		}
		$zip = new ZipArchive();
		//create zip archive
		try {
			$this->log->info('Open file "' . $zfile . '" for create zip archive');
			$zip->open($zfile, ZipArchive::CREATE | ZipArchive::OVERWRITE);
		} catch(Exception $e) {
			$this->log->error('Failed open file "' . $zfile . '" for create zip archive');
		}
		
		$logger = $this->log;
		//recursive add files
		$this->recDirOp($dir, function($err, $name, $file) use(&$logger, &$zip, &$dir) {
			if($file && !$file->isDir()) {
				$logger->info('Add file "' . $name . '" to archive.');
		    $filePath = $file->getRealPath(); // Get real and relative path for current file
		    $relativePath = substr($filePath, strlen($dir) + 1);
		    $zip->addFile($filePath, $relativePath); // Add current file to archive
		  }
		});
		
		//close zip file and log done
		try {
			$zip->close();
			$this->log->info('Create zip file "' . $zfile . '" done.');
		} catch(Exception $e) {
			$this->log->error('Close zip archive file "' . $zfile . '" failed. Error: "' . $e->getMessage() . '".');
		}
	}
	
	//get file
	private function getFile($path, $as = false) {
		try {
			$mime = mime_content_type($path);
			$file = file_get_contents($path);
			$res = ['err' => false, 'msg' => 'Soubor \'' . $path . '\' načten', 'mime' => $mime,
				'data' => $file, 'code' => 200];
		} catch (Exception $e) {
			$res = ['err' => $e, 'msg' => 'Soubor \'' . $path . '\' memohl být načten.', 'mime' => null,
				'data' => null, 'code' => 404];
		}
		$res['as'] = $as ? $as : $path;
		return $res;
	}
	
	//get key for install
	public function getKey($type) {
		$kfiles = ['private', 'cert', 'chain'];
		if(in_array($type, $kfiles)) {
			$res = $this->getFile($this->Cfg->certPath . '/' . $type . '.pem');
		} else {
			$res = ['err' => new Exception('Valid type \'' . $type . '\' of key is one from \'' . implode(', ', $kfiles) . '\''),
				'msg' => 'Neplatný typ klíče: \'' . $type . '\'. Musí být jeden z: \'' . implode(', ', $kfiles) . '\' .', 'data' => null, 'code' => 400];
		}
		return $res;
	}
	
	//get zip
	public function getZip() {
		$zfp = $this->Cfg->zipFile;
		$as = basename($this->Cfg->zipFile);
		$zf = $this->getFile($zfp, $as);
		return $zf;
	}
	
	//clear .well-known dirs (if need) after generate process
	private function clearWellKnown() {
		foreach($this->Cfg->dns as $dom) {
			$rmdir = $_SERVER['DOCUMENT_ROOT'] . $dom[1];
			if(file_exists($rmdir . '/.well-known')) {
				$this->rmRf($rmdir . '/.well-known');
			}
		}
	}
	
	//certifikate generator
	public function genCert($type) {
		$camap = [
			'testing' => $this->Cfg->testCaUrl,
			'production' => $this->Cfg->prodCaUrl
		];
		
		$lePath = $this->Cfg->lePath;
		$webRoot = $this->Cfg->webRoot;
		$dns = $this->Cfg->dns;
		
		$this->log->info("Start generate certificate for CA URL: " . $camap[$type]);
		$generated = false;
		try {
		  $le = new Lescript($camap[$type], $lePath, $webRoot, $this->log);
		  $le->initAccount();
		  $le->signDomains($dns);
		  $generated = true;
		} catch (Exception $e) {
		  $this->log->error($e->getMessage());
		  $this->log->error($e->getTraceAsString());
		  $generated = false;
		}
		$this->clearWellKnown();
		return $generated;
	}
	
	public function needBeforeRenew($ctype) {
		//Invalid param//
		if(!in_array($ctype, ['live', 'pem'])) {
			$this->log->error('Špatný parametr \'ctype\'. Musí být: \'live\' NEBO \'pem\'.');
			return false;
		}
		
		if($ctype === 'live') {
			$allow = false;
			$info = $this->getCertInfo();
			$name = 'LE certifikát je instalován na doméně: \'' . $this->Cfg->domain . '\'';
			$rules = 'nutnost obnovy live LE SSL certifikátu na doméně: \'' . $this->Cfg->domain . '\'';;
			$edesc = 'LE SSL certifikát je potřebné obnovit.';
			
			$spec = [
				'(?) doména: \'' . $this->Cfg->domain . '\' nemá platný certifikát SSL'  => ($info['err']),
				'(?) certifikát není vydán certifikační autoritou: \'' . $this->Cfg->prodCaName . '\'' => !$info['status']['isProduction'],
				'(?) doménové jméno certifikátu (CN): \'' . $info['data']['subject']['CN'] .
				  '\' není stejné jako: \'' . $this->Cfg->domain . '\'' => ($info['data']['subject']['CN'] !== $this->Cfg->domain),
				'(?) nynější datum je po: ' . date('d. m. Y – H:i:s', 
					($info['data']['validTo_time_t'] - $info['status']['tmlBefore'])) => $info['status']['needRegen']
			];
		} else if($ctype === 'pem') {
			$allow = true;
			$info = $this->getPemInfo();
			$name = 'Poslední vygenerovaný LE pem certifikát';
			$rules = 'připravenost posledního vygenerovaného LE pem certifikátu k instalaci';
			$edesc = 'soubor LE pem certifikátu je připraven k instalaci.';
			
			$spec = [
				'(?) certifikát je vygenerován' => (!$info['err']),
				'(?) certifikát je vydán certifikační autoritou: \'' . $this->Cfg->prodCaName . '\'' => $info['status']['isProduction'],
				'(?) doménové jméno certifikátu (CN): \'' . $info['data']['subject']['CN'] .
				'\' je stejné jako: \'' . $this->Cfg->domain . '\'' =>
					($info['data']['subject']['CN'] === $this->Cfg->domain),
				'(?) nynější datum je před: ' . date('d. m. Y – H:i:s', 
					($info['data']['validFrom_time_t'] + $info['status']['tmlAfter'])) => $info['status']['canInstall']
			];
		}
		
		$this->log->info('Spouštím analýzu pravidel pro: ' . $rules);
		foreach($spec as $rule => $cond) {
			$res = $cond ? 'ANO' : 'NE';
			if($ctype === 'live') {
				if($cond) $allow = true; //start false minimum 1 rule must be true
			} else if ($ctype === 'pem') {
				if(!$cond) $allow = false; //start true every rules must be true
			}	
			$this->log->info('Výsledek pro: >> ' . $rule . ' >> ' . $res);
		}
	
		$end_desc = ($ctype === 'live') ? 'Minimálně jedna podmínka je spněna' : 'Všechny podmínky jsou splněny';
		if($allow) {
			$this->log->info('Výsledek je >> SPLNĚNO << pro kontolu jestli: \'' . $end_desc . '\'');
		} else {
			$this->log->error('Výsledek je >> NESPLNĚNO << pro kontrolu jestli: \'' . $end_desc . '\'');
		}
		return $allow;
	}
	
	//certificate install
	public function installCert($validate = true, $renew = true, $install = true) {
		$can_renew = false;
		$allow_install = false;
		$installed = false;
		if($validate) {
			$this->log->info('Kontrola stavu live SSL certifikátu na doméně: ' . $this->Cfg->domain);
			$valid_live = $this->needBeforeRenew('live');
			if($valid_live) {
				$this->log->info('Live SSL certifikát na doméně: ' . $this->Cfg->domain . ' je potřeba obnovit.');
				$this->log->info('Kontrola vhodnosti posledního vygenerovaného pem SSL certifátu v souboru: \'' .
				  $this->Cfg->certPath . '/cert.pem\'');
				$valid_pem = $this->needBeforeRenew('pem');
				if($valid_pem) {
				  $allow_install = true;
					$this->log->info('Poslední vygenerovaný pem SSL certifikát je připraven pro instalaci.');
				} else {
					$this->log->error('Poslední vygenerovaný pem SSL certifikát není pro instalaci připraven.');
					if($renew) {
					  $this->log->info('Pokus o vygenerování nového pem SSL certifikátu');
					  $generated = $this->genCert('production');
					  if($generated) {
					    $this->log->info('Generování nového pem SSL certifikátu bylo úspěšně dokončeno.' .
					    ' Spouštím instalaci na hosting Wedos.');
					    $allow_install = true;
					  } else {
					    $this->log->info('Generování nového pem SSL certifikátu se bohužel nezdařilo. Zkontrolujte log.');
					  }
					}
				}
			} else {
				$this->log->error('Live SSL certifikát běžící na serveru domény zatím nepotřebuje obnovit.');
			}
		} else {
			$this->log->info('Přímá instalace posledního vygenerovaného pem SSL certifikátu bude bez ověření.');
			$allow_install = true;
		}
		
		//$this->log->info('Allow: ' . ($allow_install ? 'ON' : 'OFF') . ' Validation: ' . ($validate ? 'ON' : 'OFF'));
		if($install) {
  		if($allow_install || !$validate) {
  			$this->log->info('Instaluji LE SSL pem certifikát na doménu: ' . $this->Cfg->domain . ' 321 start ...');
  			//only can login if not locked for previous login fails (Wedos locked user if passwd is invalid repetelly)
  			if(!$this->Cfg->wedosBlocked) {
  				$auth = $this->cinst->login($this->Cfg->wedosId, $this->Cfg->wedosLogin, $this->Cfg->wedosPasswd);
  				if($auth) {
  					$cpath = $this->Cfg->certPath;
  					$pkey = $cpath . '/private.pem';
  					$cert = $cpath . '/cert.pem';
  					$chain = $cpath . '/chain.pem';
  					$installed = $this->cinst->install($pkey, $cert, $chain);
  					if($installed) {
  					  $this->log->info('Instalace certifikátu na server byla úspěšně dokončena.');
  					} else {
  					  $this->log->error('Při instalaci se vyskytly chyby, zkontrolujte log.');
  					}
  					$this->log->info('Odhlašuji se od hostingu Wedos.');
  					$this->cinst->logout();
  				} else { //block Wedos
  					$this->log->error('Přihlášení selhalo. Přihlášení k Wedos bylo proto v nastavené zablokováno.' .
  					' Opravte přihlašovací údaje a potom znovu aktivujte.');
  					$this->Cfg->wedosBlocked = true;
  				}
  			} else {
  				$this->log->error('Předchozí přihlášení k hostingu Wedos selhalo. '. 
  				'Pokud už jste opravili přihlašovací údaje k hostingu v nastavení, ' .
  				'můžete přepmout volbu [wedosBlocked] ma: \'Vypnuto\'.');
  			}
  		} else {
  			$this->log->error('Kontrolní mechanizmus zabránil instalci pem SSL certifikátu na server z výše uvedených důvodů.');
  		}
		} else {
		  $this->log->info('Instalace pem SSL certifikátu nebyla požadována.');
		}
		return $installed;
	}
	
	//send email report
	public function mailReport($message = '') {
	  $sendto = $this->Cfg->mailSendTo; //get all recepients
	  if(sizeof($sendto) === 0) {
	    return false;
	  } else if(sizeof($sendto) > 0) {
	    $to = $sendto[0];
	    $bcc = false;
	    
	    //add Bcc
	    if(sizeof($sendto) > 1) {
	      $cc = [];
	      for($i = 1; $i < sizeof($sendto); $i++) {
	        $cc[$i] = $sendto[$i];
	      }
	    }
	    
	    $from_user = $this->Cfg->mailFromUser;
	    $from_email = $this->Cfg->mailSendFrom;
	    $subject = $this->Cfg->mailSendSubject;
	    
	    $from_user = '=?UTF-8?B?' . base64_encode($from_user) . '?=';
	    $subject = '=?UTF-8?B?' . base64_encode($subject) . '?=';
	    
	    $headers = 'From: ' . $from_user . '<' . $from_email . '>' . "\r\n";
	    $headers .= (sizeof($cc > 0) ? 'Cc: ' . implode(',', $cc) . "\r\n" : '');
	  	$headers .= 'MIME-Version: 1.0' . "\r\n";
	    $headers .= 'Content-type: text/plain; charset=UTF-8' . "\r\n";
			$headers .= 'X-Mailer: PHP/' . phpversion() . "\r\n";
		  
		  return mail($to, $subject, $message, $headers);  
	  }
	}
	
	//conversion b64 char => integer || integer => b64 char
	private function ntb64c($n) {
	  $b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
	  if(gettype($n) === 'string') {
	    return strpos($b64, $n);
	  } else if(gettype($n) === 'integer') {
	    return substr($b64, $n, 1);
	  }
	}
	
	private function decToB64($dec) {
    $p = 1;  //power
    $out = '';
    while($dec > 0) {
      $rem = $dec % pow(64, $p);
      //echo "Rem: $dec % " . pow($b, $p) . " = $rem\n";
      $dig = $rem / pow(64, $p - 1);
      $dec -= $rem;
      $p++;
      $out = $this->ntb64c($dig) . $out;
    }
	  return $out;
  }
  
  private function b64ToDec($b64s) {
    $len = strlen($b64s);
    $n = 0;
    for($i = 0; $i < $len; $i++) {
      $d = $this->ntb64c($b64s[$i]);
      $n += $d * pow(64, ($len - $i - 1));
    }
    return $n;
  }
	
	//CSRF generate
	public function csrfGen($tout, $user) {
		$timeout = is_int($tout) && $tout > 0 ? $tout : 120; //default 120 sec
		$user = strval($user);
		$key = $this->Cfg->CSRFtokenKey;
		$ts = intval(date('U'));
    $sig_str = $this->decToB64($ts) . '_' . $this->decToB64($timeout);
		$mac = base64_encode(hash_hmac('sha1', $user . $sig_str, $key, true));
		return substr($mac, 0, -1) . '_' . $sig_str;
	}
	
	//CSRF verify
  public function csrfVerify($token, $user) {
    $valid = true;
    if(!$token) {
      $valid = false;
    } else {
    	$key = $this->Cfg->CSRFtokenKey;
    	$tk = explode('_', $token);
      $user = strval($user);
      //hmac, timestamp, timeout
      $mac = isset($tk[0]) ? $tk[0] : false;
      $ts = isset($tk[1]) ? $tk[1] : false;
      $tout = isset($tk[2]) ? $tk[2] : false;

      $exp = (integer)$this->b64ToDec($ts) + (integer)$this->b64ToDec($tout);
      $now = intval(time());
    	if($mac && $ts && $tout && $now < $exp) {
        $cmac = substr(base64_encode(hash_hmac('sha1', $user . $ts . '_' . $tout, $key, true)), 0, -1); //strip =
    		if($cmac !== $mac) {
    			$valid = false;
    		}
    	} else {
    		$valid = false;
      }
    }
    return $valid;
  }
}
