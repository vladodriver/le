<?php
class Cinstall {
	private $req;
	private $logger;
	
	private $wedos;
	private $w_home;
	private $w_ssl;
	private $clid;
	
	public function __construct(Req $req, Logger $logger) {
		$this->req = $req;
		$this->logger = $logger;
		
		$this->wedos = 'https://client.wedos.com';
		$this->w_home = $this->wedos . '/home/';
		$this->w_ssl = $this->wedos . '/webhosting/https.html?id=';
	}
	
	private function getWedosMessage($action, Res $res) {
		$err_msg = $res->find('~<div class="info_red".+<li>([^<]+)<\/li>~sm', 1);
		$info_msg = $res->find('~<div class="info_blue".+<li>([^<]+)<\/li>~sm', 1);
		if($err_msg) {
			$this->logger->error('Akce "' . $action . '" selhala' );
			$this->logger->error('Zpráva ze stránky: "' . $err_msg . '"');
			$res = false;
		} else if($info_msg) {
			$this->logger->info('Akce "' . $action . '" byla úspěšně dokončena' );
			$this->logger->info('Zpráva z administrační stránky: "' . $info_msg . '"');
			$res = true;
		} else {
			$this->logger->error('Není možné ověřit výsledek akce "' . $action . '" Zpráva (info/error) nebyla nalezena');
			$res = false;
		}
		return $res;
	}
	
	public function login($clid, $login, $passwd) {
		$logged = false; $this->clid = $clid;
		$this->logger->info('Přihalašuji se k uživatelskému účtu Wedos id: ' . $this->clid);
		//only headers and save non-login cookies
		$this->logger->info('Stažení hlaviček a cookie relace ze stránky ' . $this->w_home);
		$first_load = $this->req->send($this->w_home, null, false);
		if($first_load->err) {
			$this->logger->error('První požadavek na ' . $this->w_home . ' selhal. Chyba: ' .
				$first_load->err->getMessage());
		} else if($first_load->code !== 200) {
			$this->logger->error('První požadavek na ' . $this->w_home . ' selhal. Status: ' . $first_load->code);
		} else {
			$this->logger->info('Staženy hlavičky a cookies přihlašovací stránky. Status: ' . $first_load->code);
			$load_login = $this->req->send($this->w_home);
			$this->logger->info('Parsování parametru "action" pro přihlašovací formulář');
			$form_login = $load_login->find('~form.+method="post".+action="([^"]+)"~', 1);
			
			if($form_login) {
				$this->logger->info('Nalezena url přihlašovacího formuláře: ' . $form_login);
				$this->logger->info('Posílám data pro přihlášení na: ' . $form_login);
				$data = http_build_query(['login' => $login, 'passwd' => $passwd]);
				
				$auth = $this->req->send($form_login, $data);
				if($auth->err) {
					$this->logger->error('Přihlášení se nezdařilo Chyba curl: ' . $auth->err->getMessage());
				} else if($auth->code !== 200) {
					$this->logger->error('Status odpovědi přihlášení není 200 ale: ' . $auth->code);
				} else {
					$logged = $this->getWedosMessage('Přihlášení do administrace wedos', $auth);
				}
			} else {
				$this->logger->err('Url přihlašovacího formuláře se nepodařilo zjistit');
			}
		}
		return $logged; //if return false need lock... wedo lock 1 hour if many try login fails
	}
	
	//$this->msg = '~<div class="info_blue".+<li>([^<]+)<\/li>~sm'
	public function install($pkey, $cert, $chain, $pkpasswd = false) {
		$inslalled = false;
		$ssled_url = $this->w_ssl . $this->clid;
		$ssl_up = $this->req->send($ssled_url);
		$this->logger->info('Nahrávám stránku pro upload SSL certifikátů: ' . $ssled_url);
		if($ssl_up->err) {
			$this->logger->err('Požadavek na stránku pro upload SSL certifikátů selhal. Chyba curl: ' .
				$ssl_up->err->getMessage());
		} else if($ssl_up->code !== 200) {
			$this->logger->err('Požadavek na stránku pro upload SSL certifikátů selhal. Status: ' . $ssl_up->code);
		} else {
			$this->logger->info('Stránka pro nahrávání SSL certifikátů načtena Status: ' . $ssl_up->code);
			$form_ssl = $ssl_up->find('~form.+method="post".+action="([^"]+)"~', 1);
			if($form_ssl) {
				$form_ssl = $this->wedos . $form_ssl;
				$this->logger->info('Url akce formuláře nahrávání SSL klíčů: ' . $form_ssl);
				try {
					$upload = http_build_query([
						'ssl_send' => 'Provést změny',
						'ssl_variant' => 3,
						'ssl_key' => file_get_contents($pkey),
						'ssl_pkey_pass' => $pkpasswd ? $pkpasswd : '',
						'ssl_crt' => file_get_contents($cert),
						'ssl_crt_chain' => file_get_contents($chain)
					]);
					
					$this->logger->info('Klíče SSL byly úspěšně převedeny na požadavek odeslání');
					$upload = $this->req->send($form_ssl, $upload);
					if($upload->err || $upload->code !== 200) {
						$this->logger->error('Chyba při uploadu SSL klíčů. Status code: ' .
						  $upload->code . ($upload->err ? ' Curl chyba: ' . $upload->err->getMessage() : ''));
					} else {
						$installed = $this->getWedosMessage('Upload SSL klíčů na hosting wedos', $upload);
					}
				} catch(Exception $e) {
					$this->logger->error('Chyba při načtení souborů klíčů: "' . $e->getMessage() . '" !');
				}
			} else {
				$this->logger->error('Url akce formuláře nahrávání SSL klíčů nebyla nalezena');
			}
		}
		return $installed;
	}
	
	public function logout() {
	  $logout = false;
	  $this->logger->info('Provádím odhlášní na stránce: ' . $this->w_home);
	  $home = $this->req->send($this->w_home);
	  $this->logger->info('Hledám odhlašovací odkaz.');
	  $logout_link = $home->find('~a.+href="(.+?)".+class="clogout".+Odhlásit~', 1);
	  
	  if($logout_link) {
	    $this->logger->info('Odhlašovací odkaz: "' . $logout_link. '" , odhlášuji se.');
	    $loutpage = $this->req->send(htmlspecialchars_decode($logout_link));
	    if($loutpage->code === 200 && !$loutpage->err) {
	      $this->logger->info('Odhlášení úspěšně dokončeno');
	    } else {
	      $this->logger->error('Odhlášení se nezdařilo. Status code: ' .
	        $loutpage->code . ($loutpage->err ? ' Curl chyba: ' . $loutpage->err->getMessage() : ''));
	    }
	  } else {
	    $this->logger->error('Odhlašovací odkaz nebyl nalezen!');
	  }
	  return $logout;
	}
}