<?php
class Controller {
	private $Cfg;
	private $M;
	private $V;
	
	private $REQ; //for request object from App
	private $user; //for save auth user
	private $admin; //logged user is admin
	
	public function __construct(Model $model, View $view) {
		$this->M = $model;
		$this->V = $view;
	}
	
	public function __call($name, $args) {
		if(method_exists($this, $name)) {
			$req_index = sizeof($args) - 1; //last index of args is $this->req
			$this->REQ = &$args[$req_index]; //save $app->req to $this->REQ
			$this->user = isset($_SESSION, $_SESSION['username']) ?
				$_SESSION['username'] : null;
			$this->admin = isset($_SESSION, $_SESSION['admin']) ?
				$_SESSION['admin'] : false;
			unset($args[$req_index]); //delete $app->req from $args
			call_user_func_array(array($this, $name), $args);
		} else if(!preg_match('#Action#', $name)) {
			$this->sendError(
				new Exception('Method ' + $name + ' is not \'Action\' method'),
				  'Volaná akční metoda: \'' . $name . '\' není platná metoda (.*Action)', 500
			);
		} else {
			$this->sendError(
				new Exception('Method ' + $name + ' is not implemented'),
				'Metoda pro akci: \'' . $name . '\' nebyla nalezena',
				500
			);
		}
	}
	
	//send error must be public -> is called from App without __call proxy
	public function sendError(Exception $e, $msg = 'Nezpecifikovaná chyba', $code = 404, $page = true) {
		header('Cache-Control: no-cache');
		header($_SERVER["SERVER_PROTOCOL"] . " " . $code . " Error: " . $e->getMessage(), true, $code);
		
		//detect AJAX directly from $_SERVER (use without $this->REQ for csrf validation using $this->verifyCsrf)
		$ajax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] : false;
		$page = !$ajax;
		echo $this->V->printError($e->getMessage(), $msg, $code, $page);
		exit();
	}
	
	private function redirect($url) {
		header("HTTP/1.1 302 Found");
		header("Location: " . $url);
	}
	
	//action to view key file text for manual copy in https://client.wedos.com/webhosting/https.html?id=119380
	private function viewKeyAction($ktype) {
		$keyfile = $this->M->getKey($ktype);
		if($keyfile['data']) {
			header('Cache-Control: no-cache', true);
			header('Content-Type: text/plain' , true);
			echo $keyfile['data'];
		} else {
			$this->sendError($keyfile['err'], $keyfile['msg'], $keyfile['code']);
		}
	}
	
	//verify CSRF token included in POST or x-csrf-token header
	public function verifyCsrf($csrf, $user) {
	  //verify and send error response if invalid
	  if(!$this->M->csrfVerify($csrf, $user)) {
	    $this->sendError(new Exception('CSRF token is invalid'),
			  'Validace CSRF tokenu selhala', 403);
	  }
	}
	
	//add csrf header for ajax $csrf token
	private function addCsrfHeader($tout) {
	  $token = $this->M->csrfGen($tout, $this->user);
	  header('X-Csrf-Token-Update: ' . $token, true);
	}
	
	//action for download zip file //TODO bude rovnou volat getZip v modelu nebo error soubor nenalezen
	private function zipDownloadAction() {
		$zip = $this->M->getZip();
		$zipname = $zip['as'];
		$this->addCsrfHeader(200);
		if($zip['data']) {
			header('Cache-Control: no-cache', true);
			header('Content-Type: application/zip' , true);
			header('Content-Disposition: attachment; filename="' . $zipname . '"', true);
			echo $zip['data'];
		} else {
			$this->sendError($zip['err'], $zip['msg'], $zip['code']);
		}
	}
	
	//main / index action
	private function indexAction() {
		$certinfo = null; $peminfo = null;
		$peminfo = $this->M->getPemInfo();
		$certinfo = $this->M->getCertInfo();
		$cron = $this->M->Cfg->cronKey;
		$csrf = $this->M->csrfGen(600, $this->user);
		echo $this->V->index($certinfo, $peminfo, $cron, $csrf);
	}
	
	//action to view live certificate at ssl domain url installed on server
	private function infoUrlCertAction() {
		$certinfo = $this->M->getCertInfo();
		echo $this->V->printInfo($certinfo);
	}
	
	//action to view last generated pem certificate file
	private function infoPemCertAction() {
		$certinfo = $this->M->getPemInfo();
		echo $this->V->printInfo($certinfo);
	}
	
	//action generate new LE certifikates and direct log messages
	private function leGenAction($catype) {
	  $this->addCsrfHeader(200);
		if(!in_array($catype, ['production', 'testing'])) {
			return $this->M->log->error('Invalid argument of certificate type. Must be "testing" or "production"');
		}
		return $this->M->genCert($catype);
	}
	
	//installation procedure
	private function leInstallAction($validate = 'yes', $renew = 'yes', $install = 'yes') {
		//default with validation before install
		$this->addCsrfHeader(200);
		$this->M->installCert($validate === 'yes', $renew ==='yes', $install === 'yes');
	}
	
	//cron action
	private function leCronAction($key) {
	  $cronkey = $this->M->Cfg->cronKey;
	  $logfile = $this->M->Cfg->lePath . '/cert_install.log';

	  if($key === $cronkey) {
	    $need_renew = $this->M->needBeforeRenew('live'); //check if need regenerate
	 
	    if($need_renew) {
	      $this->M->log->setFile($logfile);
	      $this->M->log->info('----Instalační proces začal----');
	      $res = $this->M->installCert(true, true, true);
	      $msg = 'Automaticky generovaný log pokusu obnovení LE SSL certifikátu:' . "\n";
	      $msg .= '__________________________________________________________________' . "\n\n";
	      $msg .= file_get_contents($logfile) . "\n";
	      $msg .= '__________________________________________________________________' . "\n";
	      $this->M->mailReport($msg);
	    }
	    
	  } else {
	    $this->M->log->info('Invalid key for CRON action');
	  }
	}
	
	//action create zip archive from certificate key files
	private function zipCertFilesAction() {
	  $this->addCsrfHeader(200);
		$this->M->dirZip();
	}
	
	//used for firstUserAction and userAuthAction
	private function userAuthOperation($action) {
		$data = $this->REQ['post'];
		$login = isset($data['login']) ? $data['login'] : '';
		$passwd = isset($data['passwd']) ? $data['passwd'] : '';
		
		if($login && $passwd) {
			$auth = call_user_func_array([$this->M->U, $action], [$login, $passwd]);
			if($auth['result']['err']) {
				$err = $auth['result']['err'];
				$this->sendError($auth['result']['err'], $auth['result']['msg'] , $auth['result']['code']);
			} else {
				$this->redirect('/');
			}
		} else {
			$this->sendError(new Exception('Post auth request is not in valid format'),
					'Je třeba vyplnit uživatelské jméno a heslo', 400);
		}
	}
	
	//action login view login form
	private function userLoginAction() {
		$no_any_user = $this->M->U->usersCreated === 0;
		echo $this->V->loginForm($this->M->csrfGen(200, $this->user), $no_any_user);
	}
	
	//action logout and redirect to /
	private function userLogoutAction() {
		$this->M->U->sessionDestroy();
		$this->redirect('/');
	}
	
	//ajax fragmen logged user
	private function loggedUserAction() {
	  echo  $this->V->loggedUser(true);
	}
	
	//get register first user form (if users file not exists or not found)
	private function firstUserAction() {
		return $this->userAuthOperation('createFirstAdminUser');
	}
	
	//get authentification form
	private function userAuthAction() {
		return $this->userAuthOperation('authUser');
	}
	
	
	/************** JSON user edit Api ***************/
	//for convert Exception to array using for sendJson
	private function excToAr(Exception $e) {
		return [
			'message' => $e->getMessage(),
			'trace' => $e->getTrace(),
			'line' => $e->getLine()
		];
	}
	
	private function sendJson($res) {
		//get api response status code and convert err Exceptions to array
		$code = 200;
		if(array_key_exists('call', $res) && $res['call']['err']) {
			$code = $res['call']['code']; //error code from ['call']
			$res['call']['err'] = $this->excToAr($res['call']['err']);
		}	else if(array_key_exists('result', $res) && $res['result']) {
			$code = $res['result']['code']; //save code from api ['result']
			if($res['result']['err']) {
				$res['result']['err'] = $this->excToAr($res['result']['err']);
			}
		}
		
		//get api response error and convert Exception's to array
		$json_str = json_encode($res, JSON_UNESCAPED_UNICODE);
		header($_SERVER["SERVER_PROTOCOL"] . ' ' . $code . 'msg', true);
		header('Cache-Control: no-cache', true);
		header('Content-Type: application/json; charset=utf-8', true);
		//header('Content-Type: text/plain; charset=utf-8', true);
		echo $json_str;
	}
	
	//get json from POST, PUT or DELETE User api request and filter it
	private function jsonDataReq() {
		$req = array();
		$user = $this->user;
		$data = $this->REQ['rawdata'];
		$jsonar = json_decode($data, true);
		return $jsonar;
	}
	
	private function getValidJsonReq() {
		$jsonreq = [];
		$jsonraw = $this->jsonDataReq();
		if(is_array($jsonraw)) {
			if(array_key_exists('action', $jsonraw) && gettype($jsonraw['action']) === 'string') {
				$jsonreq['action'] = $jsonraw['action'];
				$jsonreq['params'] = isset($jsonraw['params']) && gettype($jsonraw['params']) === 'array' ?
					$jsonraw['params'] : []; //fallback - params must be set for call_user_func
			}
    }
    return $jsonreq;
	}
	
	//User Api action for POST, DELETE and PUT ajax requests
	private function editUserAction() {
		$json = $this->getValidJsonReq(); //get valid decoded and preparsed json request
		$method = $this->REQ['server']['REQUEST_METHOD'];
		$rtmap = [
			'POST' => ['createFirstAdminUser', 'authUser', 'sessionDestroy', 'getUsers', 'createUser'],
			'PUT' => ['updateUser'],
			'DELETE' => ['removeUser']
		];
		
		if($json) {
			if(in_array($json['action'], $rtmap[$method])) {
				$res = call_user_func_array([$this->M->U, $json['action']], $json['params']);
			} else {
				$res = [
					'call' => [
						'action' => $json['action'], 'params' => $json['params'], 'user' => $this->user,
						'err' => new Exception(
						  'User api request action: \'' . $json['action'] . '\' can not call with request method: \'' . $method . '\''),
						'msg' => 'Akce user api \'' . $json['action'] . '\' nemůže být volána http request metodou \'' . $method . '\'',
						'code' => 403
					], 'result' => null
				];
			}
		} else {
			$res = [
			  'call' => [
			    'action' => null, 'params' => null, 'user' => $this->user,
			    'err' => new Exception('User api request must contain minimal key: \'action\' string data type'),
				  'msg' => 'User api požadavek musí obsahovat string klíč \'action\' a volitelně klíč \'params\' typu array',
				  'code' => 400
		    ], 'result' => null
		  ];
		}
		$this->addCsrfHeader(200);
		$this->sendJson($res);
	}
	
	//list user profiles PAGE or JSON by logged user rights (all || myself)
	private function listUsersAction($ajax = 'no') {
		$user = $this->user;
		$adm = $this->admin;
		
		$action = $ajax === 'yes' ? 'printUserList' : 'viewUserProfiles';
		$res = call_user_func_array([$this->M->U, 'getUsers'], $adm ? [] : [$user]);
		$csrf = $this->M->csrfGen(200, $this->user);
		echo call_user_func_array([$this->V, $action], [$res, $csrf]);
	}
	
	//show configuration editor
	private function editConfAction() {
		$csrf = $this->M->csrfGen(600, $this->user);
		echo $this->V->viewAllConf($this->M->Cfg->getAll(), $csrf, false);
	}
	
	//save configuration changes
	private function saveConfAction() {
	  $admin = $this->admin; //must be admin
		$data = $this->REQ['post'];
		if($admin) $this->M->Cfg->setAll($data, true);
		$csrf = $this->M->csrfGen(600, $this->user);
		echo $this->V->viewAllConf($this->M->Cfg->getAll(), $csrf, true);
	}
}
