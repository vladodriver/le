<?php
class Logger {
	private $bf;
	private $file;
	
	public function __construct() {
		$this->file = false;
    header('X-Accel-Buffering: no', true);
	}
	
	public function setFile($file) {
	  $this->file = $file;
	  if(file_exists($this->file)) {
	    unlink($this->file);
	  }
	  echo '<pre class="info_log_msg msg"><b>Log will be written in to file:</b> ' . $this->file . "</pre>\r\n";
	}
	
	public function __call($name, $arguments) {
		//session_write_close();
		$base = date('Y-m-d H:i:s') . '.' .
			str_pad((string)round(explode(' ', microtime())[0] * 1000), 3, '0');
		$cont = '[' . $name . '] ' . $arguments[0];
				 
		if($this->file) {
		  file_put_contents($this->file, $base . ' -> ' . $cont . "\r\n", FILE_APPEND); 
		}
		echo '<pre class="' . $name . '_log_msg msg"><b>' . $base .
			  '</b> ' . htmlspecialchars($cont, ENT_QUOTES) . '</pre>' . "\r\n";
		if(ob_get_level() > 0) ob_flush();
		flush();
	}
}
