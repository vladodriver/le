<?php
class View {
	private $certinfo;
	private $peminfo;
	private $mainMenu;
	private $end;
	
	public function __construct() {
	  $this->mainMenu = array(
		  ['url' => '/', 'title' => 'Správa LE certifikátů', 'text' => 'Certifikáty'],
		  ['url' => '/conf', 'title' => 'Odborné nastavení aplikace', 'text' => 'Nastavení'],
		  ['url' => '/user/list', 'title' => 'Správa profilů uživatelů', 'text' => 'Uživatelé']
	  );
	  $this->end = $this->footer() . '</body></html>';
	}
	
	private function absurl($path) {
		$proto = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
		$host = $_SERVER['HTTP_HOST'];
		$path = preg_replace('~^([^\/].+)$~', '/$1', $path); //add '/' if need
		return $proto . $host . $this->esc($path);
	}
	
	private function href($url, $text, $title, $class = false, $id = false) {
		return '<a href="' . $this->esc($url) . '" title="' . $this->esc($title) . '" ' .
			($class ? ' class="' . $this->esc($class) . '"' : '') .
			($id ? ' id="' . $this->esc($id) . '"' : '') . '>' . $this->esc($text) . '</a>';
	}
	
	private function esc($str) {
		return htmlspecialchars($str, ENT_QUOTES | ENT_HTML5);
	}

	private function menu(array $items) {
		$auth_url = ($this->auth() ? '/logout' : '/login');
		$tit_text = ($this->auth() ? 'Odhlášení' : 'Přihlášení');
		$auth = ['url' => $auth_url, 'title' => $tit_text, 'text' => $tit_text];
		array_push($items, $auth);
		
		$m = '<nav id="menu"><ul>';
		foreach($items as $it) {
		  $req_path = parse_url($_SERVER['REQUEST_URI'])['path'];
			$active = $req_path === $it['url'];
			$m .= '<li>' . $this->href($this->absurl($it['url']), $it['text'], $it['title'], ($active ? 'menu_active' : '')) . '</li>';
		}
		$m = $m . '</ul></nav>';
		return $m;
	}
	
	private function auth() {
		if(isset($_SESSION) && array_key_exists('username', $_SESSION)) {
			return [
				'as' => $_SESSION['username'], 'admin' => $_SESSION['admin'],
					'la' => $_SESSION['LAST_ACTIVITY'], 'expire' => $_SESSION['EXPIRE']
			];
		} else {
			return null;
		}
	}
	
	private function parseTimestamp($seconds) {
    $z = new DateTime("@0");
    $e = new DateTime("@$seconds");
    $rk = ['d', 'h', 'm', 's'];
    $res = [];
    foreach(['a', 'h', 'i', 's'] as $i => $f) {
    	$res[$rk[$i]] = $z->diff($e)->format('%' . $f);
    }
    return $res;
	}
	
	private function formatTimestamp($ts) {
		$tso = $this->parseTimestamp($ts);
		return '<b>' . $tso['d'] . '</b> dní <b>' . $tso['h'] . '</b> hodin <b>' . $tso['m'] .
				'</b> minut <b>' . $tso['s'] . '</b> sekund';
	}
	
	private function updateInfo(array $certinfo, array $peminfo) {
		$this->certinfo = $certinfo;
		$this->peminfo = $peminfo;
	}
	
	private function addCssJs(array $cssjs, $type) {
		$out = '';
		foreach($cssjs as $url) {
			if($type === 'css') {
				$out = $out . '<link rel="stylesheet" href="' . $this->esc($url) . '">';
			} else if($type === 'js') {
				$out = $out . '<script src="' . $this->esc($url) . '"></script>';
			}
		}
		return $out;
	}
	
	private function addCsrf($token) {
	  return '<meta name="csrf-token" content="' . $token . '" id="csrf-token">';
	}
	
	private function head($title = 'Home', $css = false, $js = false, $csrf = false) {
		$title = $title ? $title : 'Home';
		
		$css = $css ? $this->addCssJs($css, 'css') : '';
		$js = $js ? $this->addCssJs($js, 'js') : '';
		$csrf = $csrf ? $this->addCsrf($csrf) : '';
		return '<!DOCTYPE html><html lang="cs"><head>' . $css . $js . $csrf .
		  '<meta name="viewport" content="width=device-width, initial-scale=1">' .
			'<title>' . $title . '</title></head><body>';
	}
	
	private function footer() {
	  return $this->createMsg('Aktuální verze aplikace je ke stažení na: ' . 
	    $this->href('https://bitbucket.org/vladodriver/le-web-manager',
	    'bitbucked.org ←↓→', 'link na git repo'), 'popup msg_ok_blue no_close', 'footer');
	}
	
	public function loggedUser($ajax = false) {
	  if($this->auth()['as']) {
	    $msg = 'Přihlášený uživatel: <b>' . $this->esc($this->auth()['as']) . '</b>. Relace vyprší: <b>' .
	    date('d.m.Y H:i:s', $this->auth()['expire']) . '</b>';
	  } else {
	    $msg = 'Žádný uživatel <b>není</b> přihlášen';
	  }
	  return $ajax ? $msg : $this->createMsg($msg, 'msg_ok_green', 'user_info');
	}
	
	private function userCan($amsg, $umsg) {
		$admin = $this->auth()['admin'];
		$cls = $admin ? 'ok_green' : 'err';
		$is = $admin ? '<b>je admin</b>' : '<b>není admin</b>';
		$can = $admin ? $amsg : $umsg;
		$msg = 'Přihlášený uživatel ' . $is . ' ' . $can;
		return $this->createMsg($msg, 'msg_' . $cls);
	}
	
	private function createMsg($msg, $clstype, $id = false) {
	  $title = preg_match('~no_close~', $clstype) ? 'Toto oznámení nelze zavřít' : 'Zavřít oznámení';
	  $res = '<div' . ($id ? ' id="' . $id . '"' : '') . ' class="popup ' . 
	    $clstype . '" title="' . $title . '">' . $msg . '</div>';
	  return $res;
	}
	
	private function getCronUrl($cronkey) {
	  return $this->absUrl('/cron?key=' . $cronkey);
	}
	
	public function index(array $certinfo, array $peminfo, $cronkey, $csrf) {
		$this->updateInfo($certinfo, $peminfo);
		
		return $this->head('LE správa a generování certifikátů', ['/css/index.css'], ['/js/functions.js', '/js/index.js'], $csrf) .
			$this->menu($this->mainMenu) . $this->loggedUser() .
			'<h2>Certifikát LE běžící na doméně</h2>' .
			$this->printInfo($this->certinfo, false) .
			'<h2>Poslední vygenerovaný certifikát LE</h2>' .
			$this->printInfo($this->peminfo, false) .
			'<h2>Generování instalace a archivace</h2>' .
			'<div id="gen_control"><input id="test" type="button" value="Testovací">' .
			'<input id="prod" type="button" value="Produkční">' .
			'<input id="install" type="button" value="Instalovat">' .
			'<input id="zipkeys" type="button" value="Zipovat">' .
			'<input id="showlog" type="button" value="Log"></div>' .
			'<div id="cgen" title="Kliknutím zavřete výpis" data-hidden="true" >' .
			'<pre class="info_log_msg msg">Log je prázdný, musíte spustit nějakou akci.</pre></div>' .
		  '<p>Generování, kontrolu a obnovu LE certifikátu naplánujete pomocí CRON URL: ' . 
			'</p><p><b class="cron_sh" title="Adresa cron akce automatického generování, kontroly a obnovy LE certifikátů">' 
			. $this->getCronUrl($cronkey) . '</b></p>' .
			'<h2>Stažení vygenerovaného certifikátu</h2>' .
			'<p>Soubory LE certifikátu můžete <span id="zipdown" title="Stáhnout soubory klíčů">zabalit a stáhnout</span> v archivu zip.</p>' .
			'<h2>Zobrazení a ruční instalace</h2>' .
			'<div id="key_change"><input id="pkey" type="button" value="Privátní klíč"><input id="cert" type="button" value="Certifikát">' .
			'<input id="chkey" type="button" value="Certifikační řetěz"></div>' .
			'<h3 id="key_type">Vyberte klíč k zobrazení</h3>' .
			$this->printCview($this->peminfo) .
			'<ol><li>Pro ruční instalaci je třeba být přihlášený do administrace hostingu <a target="_blank" href="https://client.wedos.com/home">WEDOS</a></li>' .
			'<li>Obsahy klíčů je třeba zkopírovat do správných polí v <a target="_blank" href="https://client.wedos.com/webhosting/https.html?id=119380">administraci HTTPS</a></li>' .
			'<li>Pole pro heslo <b>musí zůstat prázdné!</b></li>' .
			'<li>Testovací certifikát vydaný: <b>Fake LE Intermediate X1</b> neinstalujte nikdy na server</li>' .
			'<li>Produkční certifikát je možné generovat jen 5x v jednom týdnu, tedy 5. certifikát směrem do minulosti mussí být starší než týden</li>' .
			'<li>Výpis vydaných certifikátů <a target="_blank" href="https://crt.sh/?q=' . ($this->certinfo['data'] ?
				$this->certinfo['data']['subject']['CN'] : '') . '">zde</a></li></ol>' .
			$this->end;
	}
	
	public function printInfo($info, $update = true) {
		$prod = $info['status']['isProduction'];
		$inf_id = $info['status']['role'] . '_cert';
		$inf_class = $prod ? 'certinfo cert_prod' : 'certinfo cert_test';
		
		if($info['data'] && !$info['err']) {
			$issuer = $info['data']['issuer']['CN'];
			$for = $info['data']['subject']['CN'];
			$born = $info['data']['validFrom_time_t'];
			$dead = $info['data']['validTo_time_t'];
			$now = date('U');
		
			$live = $this->formatTimestamp($dead - $born);
			$rem = $this->formatTimestamp($dead - $now);
			
			
			$res = ($update ? '' : '<div class="' . $inf_class . '" id="' . $inf_id . '">') .
				'<p>Vydáno CA: <b>' . $issuer . '</b></p>' .
				'<p>Pro doménu: <b>' . $for . '</b></p>' .
				'<p>Důvěryhodná certifikační autorita: <b>' . ($info['status']['isProduction'] ? 'Ano' : 'Ne') . '</b></p>' .
				'<p>Délka života: ' . $live . '</p>' .
				'<p>Zbývá času: ' . $rem . '</p>' .
				'<p>Platnost od: <b>' . date('d. m. Y – H:i:s', $born) . '</b></p>' .
				'<p>Platnost do: <b>' . date('d. m. Y – H:i:s', $dead) . '</b></p>' .
				'<p>Instalovat do: <b>' . date('d. m. Y – H:i:s', $born + $info['status']['tmlAfter']) . '</b></p>' .
				'<p>Obnovit po: <b>' . date('d. m. Y – H:i:s', $dead - $info['status']['tmlBefore']) . '</b></p>'  .
				'<p>' . ($update ? '' : '</p></div>');
		} else {
			$res = ($update ? '' : '<div class="certinfo cert_err" id="' . $inf_id . '">') .
			  '<ol><li>Informace o certifikátu se nepodařilo zjistit</li>' .
			  '<li>Použijte generovací tlačítka <b>Testovací</b>, <b>Produkční</b></li>' .
			  '<li>Vyplňte autentifikační údaje v nastavení</li>' .
			  '<li>Poté přepněte volbu <b>wedosBlocked</b> na <b>Vypnuto</b> a použijte tlačítko <b>Instalovat</b></li></ol>'
			  . ($update ? '' : '</div>');
		}
		return $res;
	}
	
	private function printCview($info) {
		return '<div id="cview" data-hidden="true" data-ca-type="' .
			(preg_match("#Let's Encrypt Authority X3#", $info['data']['issuer']['CN']) ? "prod" : "test")  . '"></div>';
	}
	
	public function loginForm($csrf, $first = false) {
	  if($this->auth()) {
	    $title = 'Uživatel ' . $this->esc($this->auth()['as']) . ' je již přihlášen';
	    $res = '<h2>Přihlásit se</h2>' . $this->createMsg('Uživatel <b>' .
	      $this->esc($this->auth()['as']) . 
	      '</b> je již přihlášen! Musíte se <a href="' . $this->absUrl('/logout') .
	      '">odhlásit</a>, když se chcete přihlásit jako někdo jiný', 'msg_ok_yellow') . $this->end;
	  } else {
	    $title = $first ? 'Vytvoření prvního uživatele' : 'Přihlásit se';
	    $res = ($first ? '<h2>Vytvoření administrátora</h2>' : '<h2>Přihlášení uživatele</h2>') .
	    $this->createMsg($first ? 'Nyní můžete vytvořit první účet uživatele administrátora' :
	      'Máte li uživatelský účet, můžete se přihlásit zde', 'msg_ok_yellow') .
			  '<form method="post" action="' . ($first ? '/firstuser' : '/auth') . '">' .
			  '<h3>Jméno:</h3><input type="text" name="login" value="">'. 
			  '<h3>Heslo:</h3><input type="password" name="passwd" value="">' .
			  '<input id="csrf" type="hidden" name="csrf" value="' . $csrf . '">' .
			  '<input type="submit" id="auth" value="' . ($first ? 'Vytvořit' : 'Přihlásit') . '">' .
			  '</form>' . $this->end;
	  }
	  return $this->head($title, ['/css/index.css'], ['/js/functions.js', '/js/error.js']) .
			$this->menu($this->mainMenu) . $this->loggedUser() . $res;
	}

	public function printAddUSer() {
	  $admin = $this->auth()['admin'];
	  if($admin) {
		$res = '<h2>Přidání nového uživatele</h2><table id="user_add"><thead><tr><th>Jméno</th><th>Heslo</th><th>Admin</th></tr></thead>' .
			'<tbody><tr><td><input type="text" id="add_username" placeholder="Uživatelské jméno"></td>' .
			'<td><input type="password" id="add_passwd" placeholder="Autentifikační heslo"></td>' .
			'<td><input type="checkbox" id="add_admin"></td></tr>' .
			'<tr><td colspan="3"><input type="button" id="add_user" value="Vytvořit nového uživatele"></td></tr></tbody></table>' .
			$this->createMsg('Pro vložení uživatele vyplňte <b>Jméno</b>, <b>Heslo</b> a <b>oprávnění</b>', 'msg_ok_yellow');
	  } else {
	    $res = '';
	  }
	  return $res;
	}
	
	public function printUserRow($user, $hash, $admin) {
		$asadmin = $this->auth()['admin'];
		//$admedit = $canremove = $asadmin ? '' : 'disabled="disabled"';
		$data_for = 'data-for-user="' . $this->esc($user) . '"';
		
		return '<tr><td>' . $this->esc($user) . '</td>' . 
		  '<td><input class="edit_passwd" ' . $data_for . ' size="20" type="password" placeholder="' .
		  $this->esc($hash) . '"><td><input type="checkbox" ' . $data_for .
		  ' class="edit_admin" ' . ($admin ? 'checked="checked"' : '')  . ' ' . ($asadmin ? '' : 'disabled="disabled"') . '></td>' .
		  '<td><input class="edit_save" type="button" ' . $data_for . ' value="Uložit">' . 
		  ($asadmin ? '<td><input type="button" class="edit_remove" ' . $data_for . ' ' .
		  ' value="Smazat"></td></tr>' : '');
	}
	
	public function printUserList(array $data) {
		$admin = $this->auth()['admin'];
		if($data['call']['err']) { //call error
			$ulist = $this->createMsg($data['call']['msg'], 'msg_err');
		} else if($data['result']['err']) { //result error
			$ulist = $ulist = $this->createMsg($data['result']['msg'], 'msg_err');
		} else { //result OK
			$ulist = '<!--user list start -->';
			if(array_key_exists('data', $data['result']) &&
			  is_array($data['result']['data']) && sizeof($data['result']['data']) > 0) { //any users found
				$ulist .= '<table id="user_list"><thead><tr><th>Jméno</th><th>Heslo</th>' .
			  '<th>Admin</th><th>Změnit</th>' .
				($admin ? '<th>Odstranit</th>' : '') . '</tr></thead><tbody>';
				foreach($data['result']['data'] as $user => $crd) {
					$ulist = $ulist . $this->printUserRow($user, $crd['hash'], $crd['admin'], $admin);
				}
				$ulist .= '</tbody></table>';
			} else { //no any users found
				$ulist = $ulist . $this->createMsg('Nenalezeni žádní uživatelé, pravděpodobně byli všichni odstraměmi včetně vás!');
			}
			$ulist .= '<!-- user list end -->';
		}
		return $ulist;
	}
	
	public function viewUserProfiles(array $res, $csrf) {
		$admin = $this->auth()['admin'];
		return $this->head('Administrace uživatelů', ['/css/index.css'], ['/js/functions.js', '/js/profile.js'], $csrf) .
			$this->menu($this->mainMenu) .
			$this->loggedUser() .
			'<h2>Administrace uživatelů</h2>' .
			$this->userCan(
				'a při maximální opatrnosti může <b>mazat</b>, <b>vytvářet</b>, a <b>měnit</b> všechny uživatele',
				'a může pouze <b>měnit své heslo</b>.'
			  ) . '<div id="user_profiles">' . $this->printUserList($res) . '</div>' .
			  $this->createMsg('<b>Uložit</b> mění heslo.' . ($admin ? ' <b>Smazat</b> maže uživatele' : ''), 'msg_ok_yellow') .
			    $this->printAddUser() . $this->end;
	}
	
	public function viewAllConf(array $cnf, $csrf, $post = false) {
		$admin = $this->auth()['admin'];
		$sp = $cnf['ckeys'];
		$bc = $cnf['bc'];
		
		$h = $this->loggedUser();
		$h .= '<h2>Nastavení konfiguračních voleb</h2>';
		$h .= $this->userCan('a při maximální opatrnosti <b>může měnit konfigurační volby</b>',
			'a bohužel <b>nemůže měnit konfigurační volby</b>.');
		$h .= $this->createMsg('Kliknutím na název položky nastavení zobrazíte nápovědu', 'msg_ok_yellow');
		$h .= $this->formatConfChanges($cnf['log'], $post);
		$h .= '<form method="post" id="conf_form" action="' . $this->absurl('/conf/save') . '">';
		foreach($sp as  $key => $data) {
			$type = gettype($data['default']);
			$val = $bc[$key];
			$imp = '<div class="conf_row" id="row_' . $key . '">' .
			'<h3 class="conf_key" id="he_' . $key . '">' . $key . '</h3>';
			$imp .= $this->createMsg('<b> (?) </b><em>' . $this->esc($data['desc']) . '</em>', 'msg_ok_yellow conf_desc', 'desc_' . $key);
			
			$dis = $admin ? '' : ' disabled="disabled"';
			if($type === 'string') {
			  $itype = preg_match('~passwd$~i', $key) ? 'password' : 'text';
				$imp .= '<input type="' . $itype . '" class="conf_text" ' . $dis . ' name="' . $key . '" value="' . $this->esc($val) . '">';
			} else if($type === 'integer') {
				$imp .= '<input type="number" class="conf_num" ' . $dis . ' name="' . $key . '" value="' . $this->esc($val) . '">';
			} else if($type === 'boolean') {
				foreach(['false', 'true'] as $st) {
					$txt = $st === 'false' ? ' Vypnuto ' : ' Zapnuto ';
					$chck = (($val === true && $st === 'true') || ($val === false && $st === 'false')) ? 'checked="checked"' : '';
					$imp .= '<b>' . $txt . '</b><input type="radio" class="conf_bool" ' . $dis . ' name="' .
					  $key . '" value="' . $st . '" ' . $chck . '>';
				}
			} else if($type === 'array') {
				$start = '<div class="conf_multi_input"><input type="button" ' . $dis . ' class="conf_multiadd_minus" name="-" value="-">' .
					'<input type="text" class="conf_multitext" ' . $dis . ' name="' . $this->esc($key) . '[]" value="';
				$end = '<input type="button" ' . $dis . ' class="conf_multiadd_plus" name="+" value="+"></div>';
				if(sizeof($val) > 0) {
					foreach($val as $v) {
						$imp .= $start . $this->esc($v) . '">' . $end;
					}
				} else {
					$imp .= $start . '">' . $end;
				}
			}
			$imp .= '</div>';
			$h .= $imp;
		}
		$h .= '<input id="csrf" type="hidden" name="csrf" value="' . $csrf . '">';
		$h .= '<input id="save_conf" ' . $dis . ' type="submit" value="Uložit konfiguraci"></form>';
		
		return $this->head('Nastavení aplikace', ['/css/index.css'], ['/js/functions.js', '/js/conf.js']) .
			$this->menu($this->mainMenu) . $h . $this->end;
	}
	
	private function getStrVal($v) {
		$type = gettype($v);
		$val = $v;
		if($type === 'integer' || $type === 'string') {
			$val = strval($v);
		} else if($type === 'boolean') {
			$val = $v === false ? 'false' : 'true';
		} else if($type === 'array') {
			$val = '[' . implode(', ', $v) . ']'; 
		}
		return $val;
	}
	
	private function formatConfChanges(array $ch, $post) {
	  $cont = '<div id="conf_changes">';
		$h = '';
		if(sizeof($ch['changes']) > 0 || sizeof($ch['errors']) >0) {
			foreach($ch['changes'] as $chng) {
			  if($post) {
			    $chmsg = '</b> byl změněn z: <b>' . $this->getStrVal($chng['change']['from']) .
			      '</b> na: <b>' . $this->getStrVal($chng['change']['to']) . '</b>';
			  } else {
			    $chmsg = '</b> byl prvně nastaven na: <b>' . $this->getStrVal($chng['change']['to']) . '</b>';
			  }
				$h .= $this->createMsg('Klíč: <b>' . $chng['key'] . $chmsg, 'msg_ok_blue');
			}
			
			foreach($ch['errors'] as $errs) {
				$h .= $this->createMsg('Klíč: <b>' . $errs['key'] . '</b> hodnota: <b>' . 
					$this->getStrVal($errs['err']['val']) . '</b>' .
					($post ? ' nemůže být změněna protože: ' : ' byla resetována protože: ') .
					$errs['err']['msg'], 'msg_err');
			}
		} else {
			$h .= ($post ? $this->createMsg('Nebyly provedeny žádné změny', 'msg_err') : '');
		}
		return $cont . $h . '</div>';
	}
	
	public function printError($errmsg, $umsg, $code, $page = false) {
	  $title = 'A jéje.. Máme tu problém.';
	  $res = '<p>' . $umsg . '</p><p><b>Technicky</b>: <em>' . $this->esc($errmsg) . '</em></p>'; 
		return ($page ? $this->head($title, ['/css/index.css'], ['/js/functions.js', '/js/error.js']) .
		  $this->menu($this->mainMenu) . $this->loggedUser() . '<h2>' . $title . '</h2>' : '') .
			$this->createMsg($res, 'msg_err no_close') . ($page ? $this->end : '');
	}
}