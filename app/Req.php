<?php
class Req {
	private $ch;
	private $ckfile;
	private $ua;
	private $tout;
	
	public function __construct($tout = 30, $ckf = 'cookies.txt',
		$ua = "Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0"
		) {
	  
	  $this->ch = curl_init();
	  $this->ckfile = sys_get_temp_dir() . "/" . $ckf;
	  $this->ua = $ua;
	  $this->tout = $tout;
	 
	 	curl_setopt($this->ch, CURLOPT_TIMEOUT, $this->tout); //follow redirect
	  curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, false); //no verify CN in SSL cert
	  curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false); //skip SSL certificate verifycation
	  curl_setopt($this->ch, CURLOPT_COOKIEJAR, $this->ckfile); //save cookies to file
	  curl_setopt($this->ch, CURLOPT_COOKIEFILE, $this->ckfile); //load file 
	  curl_setopt($this->ch, CURLOPT_USERAGENT, $this->ua); //set user agent
	  curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true); //return page text
	  curl_setopt($this->ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']); //referrer
	  curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true); //follow redirect
	  
	  
	  curl_setopt($this->ch, CURLOPT_HEADER, true);
	}
	
	public function __destruct() {
	  curl_close($this->ch);
	  if(file_exists($this->ckfile)) {
	    unlink($this->ckfile);
	  }
	}
	
	public function getInfo() {
		return curl_getinfo($this->ch);
	}

	public function send($url, $data = null, $body = true) {
	  curl_setopt($this->ch, CURLOPT_NOBODY, !$body);
	  curl_setopt($this->ch, CURLOPT_URL, $url);

	  //for post
	  if ($data && gettype($data) === "string" && strlen($data) > 0) {
	    curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($this->ch, CURLOPT_POST, true);
	    curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);
	  } else {
	  	curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "GET");
	    curl_setopt($this->ch, CURLOPT_POST, false);
	    curl_setopt($this->ch, CURLOPT_POSTFIELDS, false);
	  }
		
		$cres = curl_exec($this->ch);
		if($cres) {
			$header_size = curl_getinfo($this->ch, CURLINFO_HEADER_SIZE);
			$res_err = null;
		} else {
			$res_err = new Exception('CURL error: ' . curl_errno($this->ch) .
				": " . curl_error($this->ch));
			$header_size = 0;
		}
		
		$res_code = curl_getinfo($this->ch,  CURLINFO_HTTP_CODE);
		return new Res($res_err, $res_code, $cres, $header_size);
	}
}