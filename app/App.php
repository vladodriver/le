<?php
set_error_handler(function($severity, $message, $file, $line) {
	throw new ErrorException($message, $severity, $severity, $file, $line);
});

spl_autoload_register(function ($class_name) {
	//classes for views, models and controlers
	$path = getcwd() . '/../app/' . $class_name . '.php';
	if(file_exists($path)) {
		require $path;
	}
});

class App {
	private $C;
	private $routesOk = true;
	private $routes = []; //routes;
	
	private $req; //req
	
	public $version = '1.2';
	
	public function __construct(Controller $controler) {
		$this->C = $controler;
		
		$raw_data = file_get_contents('php://input');
		$data = array();
		parse_str($raw_data, $data);
		
		$this->req = [
			'headers' => $this->getHeaders(),
			'server' => &$_SERVER,
			'get' => &$_GET,
			'post' => &$_POST,
			'cookie' => &$_COOKIE,
			'files' => &$_FILES,
			'rawdata' => $raw_data,
			'data' => $data
		];
	}
	
	public function __destruct() {
		restore_error_handler();
	}
	
	//validate added route
	public function routeValidator($method, $path, $action, $secure) {
		if(!$this->routesOk) return;
		
		$actionMethod = $action . 'Action';
		$formats = array(['string', 'array'], 'string', 'string', 'boolean');
		$args = func_get_args();
		
		$err = false;
		//format route params validation
		foreach($args as $i => $ar) {
			if(gettype($formats[$i]) === 'string') {
				if(gettype($ar) !== $formats[$i]) {
					$err = [$i + 1 . '. parameter on route definition: "' . $path . '" is not "' . $formats[$i] . '" !',
						$i . '. parametr v definici cesty "' . $path . '" není typ: "' . $formats[$i] . '".'];
				}
			} else if(gettype($formats[$i]) === 'array') {
				if(!in_array(gettype($ar), $formats[$i])) {
					$err = [$i + 1 . '. parameter on route definition "' . $path . '" is not any of "' . implode(', ', $formats[$i]) . '".',
						$i + 1 . '. parametr v definici cesty "' . $path . '" není žádný z typů: "' . implode(', ', $formats[$i]) . '".'];
				}
			}
		}
		
		$http_methods = ['GET', 'POST', 'PUT', 'DELETE'];
		$mt = [];
		if(gettype($method) === 'string') {
		  $mt = [$method];
		} else if(gettype($method) === 'array') {
			$mt = $method;
			$bad_method = false;
			foreach($mt as $mstr) {
				if(!in_array($mstr, $http_methods)) {
					$bad_method = $mstr;
				}
			}
			if($bad_method) {
				$err = ['HTTP request method: "' . $bad_method . '" not allowed, because not included in:  "' . implode(', ', $http_methods) . '".',
					'HTTP metoda: "' . $bad_method . '" není platná, musí být jedna z: "' . implode(', ', $http_methods) . '".'];
			}
		}
		
		if(!preg_match('#^\/.*$#', $path)) {
			$err = ['Route path definition must be string started at "/" character.', 
				'Definice cesty v route musí být řetězec začínající znakem "/".'];
		} else if(!method_exists($this->C, $actionMethod)) {
			$err = ['Class method: "' . $actionMethod . '" not exists in Controller class.', 
				'Požadovaná controler metoda: "' . $actionMethod . '" v routeru neexistuje v třídě Controler.'];
		}
		
		if($err) {
			$this->routesOk = false;
			$this->C->sendError(new Exception($err[0]), $err[1], 500);
		} else {
			$this->routesOk = true;
		}
	}
	
	//add one static routes
	public function route($method, $path, $action, $secure = false, $csrf = false) {
		if(!$this->routesOk) return;
		
		$this->routeValidator($method, $path, $action, $secure);
		$re = new ReflectionMethod($this->C, $action . 'Action');
		$params = array();
		$re_params = $re->getParameters();
		
		//add info about name and optioanlity of parameters
		foreach($re_params as $par) {
			$name = $par->getName();
			$params[$name] = !$par->isOptional();
		}
		
		//add one route
		$this->routes[$path] = array(
			'method' => $method, //string|array
			'action' => $action, //string
			'params' => $params, // -> from reflection
			'secure' => $secure, //false|true
			'csrf' => $csrf      //false|true
		);
	}
	
	//get array from query
	private function getQuery() {
		$method = $this->req['server']['REQUEST_METHOD'];
		$request = array();
		$request['method'] = $this->req['server']['REQUEST_METHOD'];
		$request['query'] = $this->req['get'];
		return $request;
	}
	
	//validate query
	private function paramValidator(array $params) {
		$request = $this->getQuery();
		$req_method = $request['method'];
		$req_params = $request['query'];
		
		$invalid = array();
		$valid = array();
		
		foreach($params as $param => $required) {
			//sanitize quotes from url
			$param = htmlspecialchars($param, ENT_QUOTES);
			//required params must be in request query
			if($required && !array_key_exists($param, $req_params)) {
				array_push($invalid, $param);
			} else {
				//add optional argument if exists
				if(array_key_exists($param, $req_params)) {
					array_push($valid, $req_params[$param]);
				}
			}
		}
		return array('missing' => $invalid, 'call' => $valid);
	}
	
	//validate allowed method
	private function disallowMethodValidator($method) {
		$req_type = $this->req['server']['REQUEST_METHOD'];
		$disallow = false;
		$bad = false;
		
		if(gettype($method) === 'string') {
			$bad = ($method !== $req_type) ? $req_type : false;
		} else if(gettype($method) === 'array') {
			$bad = (!in_array($req_type, $method)) ? $req_type : false;
		}
	
		return $bad;
	}
	
	//get all headers fallback or native
	private function getHeaders() {
	  if(!function_exists('apache_request_headers')) {
      $headers = [];
      foreach ($_SERVER as $name => $value) { 
        if (substr($name, 0, 5) === 'HTTP_') { 
          $hkw = explode('_', strtolower(substr($name, 5)));
          foreach($hkw as &$w) {
            $w = ucfirst($w);
          }
          $headers[implode('-', $hkw)] = $value;
        }
      } 
      return $headers;
	  } else {
	    return apache_request_headers();
	  }
  }
	
	public function secureValidator() {
		if(isset($_SESSION, $_SESSION['username'])) {
			return true;
		} else {
			return false;
		}
	}
	
	public function run() {
		if(!$this->routesOk) return;
		
		$headers = $this->req['headers'];
		if(array_key_exists('X-Requested-With', $headers) && $headers['X-Requested-With'] === 'XMLHttpRequest') {
			$isajax = true;	
		} else {
			$isajax = false;
		}
		
		$purl = parse_url($this->req['server']['REQUEST_URI']);
		$path = $purl['path'];
		
		//search match route name and
		foreach($this->routes as $route => $def) {
			//find matched route
			if(preg_match('#^' . $route . '\/?$#', $path)) {
				//validate request allowed method
				$disallow = $this->disallowMethodValidator($def['method']);
				if(gettype($def['method']) === 'array') {
					$meth_str = implode(', ', $def['method']);
				} else if(gettype($def['method']) === 'string') {
					$meth_str = $def['method'];
				}
				
				if($disallow) {
					return $this->C->sendError(
						new Exception('HTTP method "' . $disallow . '" not allowed for this path "' . $path .
						  '". Must be any of "' . $meth_str . '".'), 
						'HTTP metoda: "' . $disallow . '" není povolena pro cestu: "' . $path . '" Povolené jsou: "' .
						$meth_str . '".', 405, !$isajax);
				}
				
				//check if route is secure and denny if not username session variable
				if($def['secure'] && !$this->secureValidator()) {
					$proto = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
					$loginurl = $proto . $_SERVER['HTTP_HOST'] . '/login';
					return $this->C->sendError(new Exception('Access denied for path: "' . $path . '".'),
						'Přístup na: <b>' . $path . '</b> je povolen jen ' .
						'<a href="' . $loginurl . '" title="Přihlásit"><b>přihlášeným uživatelům</b></a>.',
						200, !$isajax);
				}
				
				//validate csrf routes
				if($def['csrf']) {
				  //get csrf token
      	  $csrf = null;
      	  if(isset($this->req['headers']['X-Csrf-Token'])) {
      	    $csrf = $this->req['headers']['X-Csrf-Token'];
      	  } else if(isset($this->req['post']['csrf'])) {
      	    $csrf = $this->req['post']['csrf'];
      	  }
      	  $csrf = strval($csrf);
				  $user = isset($_SESSION, $_SESSION['username']) ? $_SESSION['username'] : null;
				  $this->C->verifyCsrf($csrf, $user);
				}
				
				//validate required action params
				$valid = $this->paramValidator($def['params']);
				//missing required action parameters
				if(sizeof($valid['missing']) > 0) {
					return $this->C->sendError(new Exception('Controler required action params for route "' . $route . '" (' .
						implode($valid['missing'], ', ') . ') is missing in url query.'),
						'Povinné parametry: "' . implode($valid['missing'], ', ') . '" pro cestu: "' . $path .
						'" nejsou obbsaženy v url query.', 400, !$isajax);
				}
				
				$action = $def['action'];
				$actparams = $valid['call'];
			}
		}
		
		//no match route - action found
		if(!isset($action) && !isset($actparams) && !isset($data)) {
			$this->C->sendError(new Exception('No action for : "' . $path . '" route.'),
			  'Stránka <b>' . $this->req['server']['HTTP_HOST'] . '/' . $this->req['server']['REQUEST_URI'] .
			  '</b> nebyla nenalezena.', 404, !$isajax);
		} else {
			array_push($actparams, $this->req); //add $this->req to called controler parrams
			call_user_func_array(array($this->C, $action . 'Action'), $actparams); //call controller private method filtered using __call
		}
	}
}
