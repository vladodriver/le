<?php
class ConfSpec {
	private $r; //validation regexps
	
	public $ckeys; //specification for valid config keys
	
	public function __construct() {
		//validation regexps
		$this->r = array();
		$this->r['fqdn_2'] = '~^[^\-][a-z0-9\-]*\.{1}[a-z]{2,}$~'; //FQDN level 2
		//relative path name
		$this->r['rpath'] = '~^(?!-)[a-zA-Z0-9-]+(?<!-)(\/(?!-)[a-zA-Z0-9-]+(?<!-))*(\/(?!-\.)[a-zA-Z0-9-\.]+(?<!-\.))?$~';
		$this->r['sdir'] = '~^(?!-)[a-zA-Z0-9-]+(?<!-)$~'; //for sub domain dir
		$this->r['fname'] = '~^[a-zA-Z0-9\.\-_]+$~'; //filename using a-zA-Z0-9.-_
		$this->r['num'] = '~^[0]*([1-9]+\d*)$~'; //number
		$this->r['hex24'] = '~^[a-fA-F0-9]{48}$~'; //hex token for urls
		$this->r['base6424'] = '~^[A-Za-z0-9+/]{32}$~'; //for base64 CSRF key
		$this->r['email'] = '~^[^\s@]+@[^\s@]+\.[^\s@]{2,}$~'; //email address
		
		//configuration keys and desc.
		$this->ckeys = [
			'domain' => [
			 	'desc' => 'Nastavení doménového jména 2. řádu. Výchozí je detekováno z požadavku prohlížeče a nemělo by být nutné ji měnit.',
			 	'reg' => $this->r['fqdn_2'],
			 	'require' => true, 
			 	'default' => $this->getDomainFromHost($_SERVER['HTTP_HOST'])
			],
			'lePath' => [
				'desc' => 'Nastavení názvu adresáře který se vytvoří nad kořenovým adresářem webserveru. Výchozí název je LE.' .
					' Tam se ukládájí certifikáty, uživatelské účty a log automatické obnovy. Povolené znaky a-z0-9 a pomlčka jen uprostřed slova.',
				'reg' => $this->r['sdir'],
				'require' => true,
				'default' => 'LE'
			],
			'domainRootDir' => [
				'desc' => 'Nastavení relativní cesty ke kořenovému adresáři webu. Výchozí je nic. Může být např: domains. Ovlivněno .htaccess.' .
					' Nevyplněno znaméná přímo v adresáři webu hlavní domény. Vyplněno např: domains znamená cestu: /domains.',
				'reg' => $this->r['rpath'],
				'require' => false,
				'default' => ''
			],
			'domainDocDir' => [
				'desc' => 'Nastavení relativní cesty podadresáře pro soubory webu hlavní domény. Výchozí je nic. Může být např: www.' .
					' Ovlivněno .htaccess. Nevyplněno znaméná v kořenovém adresáři webu. Vyplněno přidává další adresáře k cestě.',
				'reg' => $this->r['rpath'],
				'require' => false,
				'default' => ''
			],
			'subDomainRootDir' => [
				'desc' => 'Nastavení relativní cesty adresáře obsahu webu pro subdomény. Vyýchozí je subdom a subdomény jsou pak v /subdom.' .
					' Ovlivněno .htaccess. Nesmí obsahovat / a - na začátku nebo konci.',
				'reg' => $this->r['rpath'],
				'require' => false,
				'default' => 'subdom'
			],
			'subDomainDocDir' => [
				'desc' => 'Nastavení relativní cesty podadresáře pro obsah webu subdomény. Vyýchozí je nic. ' .
					'Tato cesta musí být pro každou subdoménu stejná. Nevyplneno znamena, že obsah je přímo v adresáři obsahu webu pro subdomény.' .
					' Ovlivněno .htaccess. Vyplneno přidává dalši adresáře k cestě takže např: www dává cestu: /subdom/nazev_subdomeny/www',
				'reg' => $this->r['rpath'],
				'require' => false,
				'default' => ''
			],
			'subnames' => [
				'desc' => 'Nastavení seznamu názvů subdomén pro alternativní názvy DNS v certifikátu.' .
					' Ovlivněno .htaccess. Nevyplněno znaméná, že certifikát bude vydán jen pro jméno hlavní domény a www verzi.' .
					' Cesty k subdoménám na serveru musí existovat a byt smerovany v DNS a .htaccess.' .
					' Povolené znaky a-z0-9 a pomlčka jen uprostřed slova.',
				'reg' => $this->r['sdir'],
				'require' => true,
				'default' => []
			],
			'tmlAfter' => [
				'desc' => 'Nastavení meze čerstvosti pro nainstalování – maximální doby po vydání.' .
					' Výchozí je 4 * 86400, čili certifikát je vhodný k instalaci nejpozději 4 dny po vydání a méně.',
				'reg' => false,
				'require' => true,
				'default' => 4
			],
			'tmlBefore' => [
				'desc' => 'Nastavení meze stáří pro obnovu – minimální doba před expirací.' .
					' Výchozí je 4 dny, čili certifikát je potřeba obnovit nejdříve 4 dny před expirací a méně.',
				'reg' => false,
				'require' => true,
				'default' => 4
			],
			'zipFile' => [
				'desc' => 'Nastavení názvu souboru zip pro archivaci souborů certifikátu.' .
					' Výchozí je certifikaty.zip. V názvu pro soubor zip jsou povoleny jen znaky a-zA-Z0-9-_',
				'reg' => $this->r['fname'],
				'require' => true,
				'default' => 'certitikaty.zip'
			],
			'passwdFile' => [
				'desc' => 'Nastavení názvu souboru pro přihlašovací údaje uživatelů. Výchozí je users.pass' .
					' V názvu jsou povoleny znaky a-zA-Z0-9-_',
				'reg' => $this->r['fname'],
				'require' => true,
				'default' => 'users.pass'
			],
			'sessionExpire' => [
				'desc' => 'Maximální doba neaktivity uživatele v sekundách, po jejímž uplynutí je uživatel odhlášen.' .
					' Výchozí je 3600 sekund (1 hodina).',
				'reg' => $this->r['num'],
				'require' => true,
				'default' => 3600 
			],
			'secureCookie' => [
				'desc' => 'Výchozí hodnota je detekována z požadavku prohlížeče. Zapnutím se session cookie přenáší ' .
				'jen po SSL připojení, což má za následek, že bez https se nelze přihlásit. Zapnout je tedy možné' .
				' až po instalaci důvěryhodného SSL certifikátu na server.',
				'reg' => null,
				'require' => false,
				'default' => !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off'
			],
			'minUsernameLen' => [
				'desc' => 'Minimální délka uživatelského jména pro přihlašování. Výchozí je 3.',
				'reg' => $this->r['num'],
				'require' => true,
				'default' => 3
			],
			'minPasswdLen' => [
				'desc' => 'Minimální délka hesla pro přihlašování. Výchozí je 6.',
				'reg' => $this->r['num'],
				'require' => true,
				'default' => 6
			],
			'wedosId' => [
				'desc' => 'Je třeba vyplnit správné ID webhostingu wedos pro automatické nahrávání certifikátů na server.' ,
				'reg' => false,
				'require' => false,
				'default' => ''
			],
			'wedosLogin' => [
				'desc' => 'Je třeba vyplnit uživatelské jméno pro přihlášení do administrace wedos pro automatické nahrávání' .
				' certifikátů na server.' ,
				'reg' => false,
				'require' => false,
				'default' => ''
			],
			'wedosPasswd' => [
				'desc' => 'Je třeba vyplnit heslo pro přihlášení do administrace wedos pro automatické nahrávání' .
				' certifikátů na server.' ,
				'reg' => false,
				'require' => false,
				'default' => ''
			],
			'wedosBlocked' => [
				'desc' => 'Výchozí je Zapnuto. Je li zadáno správné heslo, můžete přepnout na Vypnuto. Se zapnutou volbou je zabráněno přihlášení' .
				' k administraci hostingu wedos. V případě neúspěšného pokusu o přihlášení bude tato volba automaticky aktivivána.',
				'reg' => false,
				'require' => false,
				'default' => true
			],
			'mailFromUser' => [
				'desc' => 'Jméno pro jméno odesílatele automatických emailů. Výchozí je LE certificate ROBOT.' ,
				'reg' => false,
				'require' => true,
				'default' => 'LE certificate ROBOT'
			],
			'mailSendFrom' => [
				'desc' => 'Emailová adresa odesílatele automatických emailů. Měla by existovat a být funkční. Výchozí je info@<domena>.',
				'reg' => $this->r['email'],
				'require' => true,
				'default' => 'info@' . $this->getDomainFromHost($_SERVER['HTTP_HOST'])
			],
			'mailSendTo' => [
				'desc' => 'Seznam emailových adres příjemců automatických emailů. Musí to být platné a funkční emailové adresy.',
				'reg' => $this->r['email'],
				'require' => true,
				'default' => []
			],
			'mailSendSubject' => [
				'desc' => 'Text předmětu pro odeslané automatické emaily. Je třeba mít vyplňenou nejméně jednu adresu příjemce.',
				'reg' => false,
				'require' => true,
				'default' => 'SSL Certificate install report'
			],
			'cronKey' => [
				'desc' => 'Klíč pro cron url požadavky – neměnit, závisí na něm fungování automatické obnovy certifikátů přes CRON.' .
				' Generuje se automaticky. Výchozí hodnota je 24 bitový' .
				' náhodný řetězec v hexadecimálním tvaru.',
				'reg' => $this->r['hex24'],
				'require' => true,
				'default' => bin2hex(openssl_random_pseudo_bytes(24))
			],
			'CSRFtokenKey' => [
				'desc' => 'CSRF token key – není nutno měnit, generuje se automaticky. ' .
				'Výchozí hodnota je 24 bitový náhodný řetězcec v base64.',
				'reg' => $this->r['base6424'],
				'require' => true,
				'default' => base64_encode(openssl_random_pseudo_bytes(24))
			]
		];
	}
	
	private function getDomainFromHost($hostname) {
		$m = [];
		preg_match('~^(?:.*\.)?([^\-][a-z0-9\-]*\.{1}[a-z]{2,5})(:\d{2,5})?$~', $hostname, $m);
		$dm = isset($m[1]) ? $m[1] : $hostname; //fallack if domain not level 2 ex: localhost
		return $dm;
	}
}